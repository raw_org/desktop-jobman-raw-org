package jobman.Reporter;

/*
 * JobReporter.java
 *
 * Created on 24 April 2007, 19:01
 *
 * Copyright (c) <2008>, <Robert Worrall>
 *  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */




import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import jobman.JobManGUIs.JobManager;
import JobTypes.MainJob;

/**
 *
 * @author Prime - Work
 */
public class JobReporter {
   
    private String report;
    private JobShortReporter shortReporter;
    private JobManager jobManager;
    
    /** Creates a new instance of JobReporter */
    public JobReporter() {
        report = "<html><head>";
    }
    
    /** method to create a html formated string that contains the complete description of a Job*/
    public boolean gernerateFullReport(MainJob aJob, File targetFile, JobManager aJobManager){
            jobManager = aJobManager;
        //set the pagetitle as the top levels job title
          boolean result = false;
          report = report + "</head><body>";
        //set the title on the page
          report = report + "" + aJob.getName() + "<br>";
          //set the job number under the main title on the page
          report = report + "Job Number: " + aJob.getJobNumber() + "<br>";
          // report the main job dates, time spent 
          report = report + "Start Date: " + aJob.getstartDate() +"<br>Finish Date: "+ aJob.getFinshDate() +"<br>";
          
          String jobNotes = aJob.getNotes();
          //System.out.println(jobNotes);
          // get just the text attributes from the notes 
          jobNotes = removeHTMLandBODYtags(jobNotes);
          //Create a table wich contains the job summary
          report = report + "<h3>Summary/ Main Job : </h3> <table style=\"width: 100%; text-align: left; margin-right: 0px;\" border=\"1\" cellpadding=\"2\" cellspacing=\"2\"><tbody><tr><td>" + jobNotes + "</td></tr></tbody></table><br>";
          //add additional Notes
          report += "<h4>Addiotional Notes</h4>";
          report += "<table style=\"width: 100%; text-align: left; margin-right: 0px;\" border=\"1\" cellpadding=\"2\" cellspacing=\"2\"><tbody><tr><td>" + aJob.getOtherDetails() + "</td></tr></tbody></table><br>";
          //list the number of subjobs
          List <MainJob> subJobList = jobManager.getJobs(aJob.getLinkedJobs());
        try {
            //open the target file to place the string from the report into
            FileWriter jobWriter = new FileWriter(targetFile);
            //feed the contents of the report created so far into the jobWriter
            jobWriter.write(report);
            System.out.println("Starting to create report");
            //iterate through the jobs adding to the jobwriter
            Iterator<MainJob> subJobIterator = subJobList.iterator();
            while (subJobIterator.hasNext()) {
                MainJob currentSubJob = subJobIterator.next();
                jobWriter.write(generateShortReport(currentSubJob));
            }
          //finish the document
          jobWriter.write("</body></html>");
          jobWriter.close();
          System.out.println("Finished creating report");
          /*This section is no longer relevant as reports generated were too large for a string resulting in a out of memory error
          //now get the whole report from the tempFile
          StringBuilder reportBuilder = new StringBuilder();
          FileReader fr = new FileReader(tempfile);
          int ch;
          do {
          ch = fr.read();
          if (ch != -1)
          reportBuilder.append((char)ch);
          } while (ch != -1);
          fr.close();
          System.out.println("Finshed reading report");
          report = reportBuilder.toString();*/
          //report = new FileReader(tempFile);
          result = true;
        } catch (IOException ex) {
            System.out.println("Building of report failed: " + ex);
            Logger.getLogger(JobReporter.class.getName()).log(Level.SEVERE, null, ex);
        }                                     
        return result;
    }
    
    
    /** method to generate a short report on just a given job and not it's subjobs*/
    public String generateShortReport(MainJob aJob){
    String shortReport = "";
    
    shortReport = shortReport + "" + aJob.getName() + "<br>";
          //set the job number under the main title on the page
          shortReport = shortReport + "Job Number: " + aJob.getJobNumber() + "<br>";
          // shortReport the main job dates, time spent 
          shortReport = shortReport + "Start Date: " + aJob.getstartDate() +"<br>Finish Date: "+ aJob.getFinshDate() +"<br>";
          
          String jobNotes = aJob.getNotes();
          //System.out.println(jobNotes);
          // get just the text attributes from the notes 
          jobNotes = removeHTMLandBODYtags(jobNotes);
          //Create a table wich contains the job summary
          shortReport = shortReport + "<h3>Summary/ Main Job : </h3> <table style=\"width: 100%; text-align: left; margin-right: 0px;\" border=\"1\" cellpadding=\"2\" cellspacing=\"2\"><tbody><tr><td>" + jobNotes + "</td></tr></tbody></table><br>";
          //add additional Notes
          shortReport += "<h4>Addiotional Notes</h4>";
          shortReport += "<table style=\"width: 100%; text-align: left; margin-right: 0px;\" border=\"1\" cellpadding=\"2\" cellspacing=\"2\"><tbody><tr><td>" + aJob.getOtherDetails() + "</td></tr></tbody></table><br>";
          //list the number of subjobs
          List <MainJob> subJobList = jobManager.getJobs(aJob.getLinkedJobs());
          Iterator <MainJob> subJobIterator = subJobList.iterator();
          while(subJobIterator.hasNext()){
          MainJob currentSubJob = subJobIterator.next();
           shortReport += generateShortReport(currentSubJob);
          }
    
    return shortReport;
    }
    
    /** This method trims a HTML string removing all up to the <BODY> tags*/
    private String removeHTMLandBODYtags(String jobNote){
    String result;
    int start;//variable for where to start substring
    int stop;// variable for where to finish substring
    start = jobNote.indexOf("<body>") + 6;
    stop = jobNote.lastIndexOf("</body>");
    if(start == stop){
        result = "";
    }
    else{
       try{
        result = jobNote.substring(start,stop);
       }
       catch (StringIndexOutOfBoundsException e){
       result = jobNote;
       }
    //System.out.println(result);
    }
    return result;
    }
}
