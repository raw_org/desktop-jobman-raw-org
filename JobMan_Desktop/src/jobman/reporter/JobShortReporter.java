package jobman.Reporter;

/*
 * JobShortReporter.java
 *
 * Created on 24 April 2007, 19:01
 *
* Copyright (c) <2008>, <Robert Worrall>
 *  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */




import java.util.ArrayList;
import java.util.List;
import jobman.JobManGUIs.JobManager;
import JobTypes.MainJob;

/**
 *
 * @author Prime - Work
 */
public class JobShortReporter {
    
    JobManager jobManager;

    /** Creates a new instance of JobShortReporter */
    public JobShortReporter() {
    }

    public JobShortReporter(JobManager ajobManager) {
        this.jobManager = ajobManager;
    }
    
    public String generateShortReport(MainJob aJob){
        String shortReport = "";
    
    shortReport = shortReport + "" + aJob.getName() + "<br>";
          //set the job number under the main title on the page
          shortReport = shortReport + "Job Number: " + aJob.getJobNumber() + "<br>";
          // shortReport the main job dates, time spent 
          shortReport = shortReport + "Start Date: " + aJob.getstartDate() +"<br>Finish Date: "+ aJob.getFinshDate() +"<br>";
          
          String jobNotes = aJob.getNotes();
          //System.out.println(jobNotes);
          // get just the text attributes from the notes 
          jobNotes = removeHTMLandBODYtags(jobNotes);
          //Create a table wich contains the job summary
          shortReport = shortReport + "<h3>Summary/ Main Job : </h3> <table style=\"width: 100%; text-align: left; margin-right: 0px;\" border=\"1\" cellpadding=\"2\" cellspacing=\"2\"><tbody><tr><td>" + jobNotes + "</td></tr></tbody></table><br>";
          //add additional Notes
          shortReport += "<h4>Addiotional Notes</h4>";
          shortReport += "<table style=\"width: 100%; text-align: left; margin-right: 0px;\" border=\"1\" cellpadding=\"2\" cellspacing=\"2\"><tbody><tr><td>" + aJob.getOtherDetails() + "</td></tr></tbody></table><br>";
          //list the number of subjobs
          ArrayList subJobList = aJob.getLinkedJobs();
          shortReport += "<h4>Associated SubJobs: </h4>" + listSubjobs(aJob);
          
          return shortReport;
    }
    
    private String removeHTMLandBODYtags(String jobNote){
    String result;
    int start;//variable for where to start substring
    int stop;// variable for where to finish substring
    start = jobNote.indexOf("<body>") + 6;
    stop = jobNote.lastIndexOf("</body>");
    if(start == stop){
        result = "";
    }
    else{
       try{
        result = jobNote.substring(start,stop);
       }
       catch (StringIndexOutOfBoundsException e){
       result = jobNote;
       }
    //System.out.println(result);
    }
    return result;
    
}


    private String listSubjobs(MainJob aJob) {
         String returnString = "";
        if(jobManager != null){
            ArrayList<String> linkedJobs = aJob.getLinkedJobs();
            List<MainJob> jobs = jobManager.getJobs(linkedJobs);
            for (MainJob job : jobs){
            returnString = returnString + " " + job.getJobNumber() + ": " + job.getName() + "<br>";
            }
        }

        return returnString;
    }

}
