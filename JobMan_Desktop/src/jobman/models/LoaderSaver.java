/*
 * LoaderSaver.java
 *
 * Created on 24 October 2006, 15:23
 *
* Copyright (c) <2008>, <Robert Worrall>
 *  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */

package jobman.models;

/**
 *
 * @author Prime - Work
 */
import java.util.ArrayList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;

public class LoaderSaver {
    
    /** Creates a new instance of LoaderSaver */
    public LoaderSaver() {
    }
    
   public ArrayList load(String fileName)throws IOException {
   ArrayList result = new ArrayList();
   File targetFile = new File(fileName + ".data");
   FileInputStream fileInputStream = new FileInputStream(targetFile);
   ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
   try{result = (ArrayList) objectInputStream.readObject();}
   catch (Exception e){System.out.println(e);}
   return result;
   } 
   
   public boolean save(String fileName, ArrayList data) throws IOException{
   boolean result = false;
   File targetFile = new File(fileName + ".data");
   FileOutputStream fileOutputStream = new FileOutputStream(targetFile);
   ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
   try{
   objectOutputStream.writeObject(data);
   result = true;
   }
   catch (Exception e){}
   return result;
   }
   
}