package jobman.models;

/*
 * AlertListModel.java
 *
 * Created on 19 September 2007, 21:22
 *
 * Copyright (c) <2008>, <Robert Worrall>
 *  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

 */




import java.util.ArrayList;
import javax.swing.AbstractListModel;
import java.text.SimpleDateFormat;
import JobTypes.MainJob;


/**
 *
 * @author Prime - Work
 * 
 * This class extends the AbstractListModel as custom model for the JTable used in the JobAlertDialog
 *
 */
public class AlertListModel extends AbstractListModel{
    private ArrayList <MainJob> jobList;//List of jobs to be diplayed
    private SimpleDateFormat formatter = new SimpleDateFormat(); // To format the dates in the model
   
    /** Creates a new instance of AlertListModel */
    public AlertListModel() {
        jobList = new ArrayList();
    }
    
    /** Creates a new instance of AlertListModel with a list of Jobs*/
    public AlertListModel(ArrayList aJobList){
        jobList = aJobList;
    }

    public int getSize() {
        return jobList.size();
    }

    public String getElementAt(int index) {
        String result; 
        
        MainJob currentJob = jobList.get(index);
        result = currentJob.getJobNumber() + " ";
        result = result + currentJob.getName() + ". ";
        result = result +  "Due on: "+formatter.format(currentJob.getReminder());
        return result;
    }
    
    public void addElement(MainJob aJob){
        jobList.add(aJob);
    }
}
