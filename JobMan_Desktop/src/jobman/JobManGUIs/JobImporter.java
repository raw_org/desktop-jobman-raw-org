/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jobman.JobManGUIs;


import java.io.File;
import java.util.ArrayList;
import java.util.List;
import RAWCustomSwingComponents.SpreadsheetManager;
import java.util.Arrays;

import JobTypes.MainJob;
import jobman.database.jobnumberingschemes.JobNumberingSchemeInterface;







/**
 *This class takes a file and parses it into subjobs or many main jobs
 * @author Prime - Work
 */



public class JobImporter {

private  List <MainJob> newMainJobList;
private  List <MainJob> newSubJobList;
private SpreadsheetManager spreadSheetManager;
private final JobNumberingSchemeInterface numberingScheme;


    public JobImporter(String numberingScheme) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        newMainJobList = new ArrayList<MainJob>(); 
        newSubJobList = new ArrayList<MainJob>();
        spreadSheetManager = new SpreadsheetManager();
        this.numberingScheme = (JobNumberingSchemeInterface) Class.forName(numberingScheme).newInstance();
        
    }
//place getters and setters here
    
    public List<MainJob> getNewSubJobList() {
        return newSubJobList;
    }
  
    public List <MainJob> getNewMainJobList() {
        return newMainJobList;
    } 

/*
 **Create inner classes to create prgress bars to establish what stage the import is at.
 */
//class GernateCVSTask extends SwingWorker<Void, Void> {}


/*
 ** This method returns a list of available headers as a set removing any duplicate headers.
 *  it does this by using the spreadSheetManager utility
 */
    public List<String> getAllHeaders(File[] files, int rowNumber) {

     return spreadSheetManager.getHeaders(Arrays.asList(files),rowNumber);

    }

    /*
     ** This public method sets in motion the creation of subjobs from the given criteria
     */
    public boolean importSubjobsFromCVS(List<File> aFileList, List<String> aCommonHeaderList, String jobName,List<String> jobNoteHeaders, List<String> otherDetailHeaders, int aHeaderRowNumber, MainJob currentJobEdit) {
        //boolean success = false;
        //first try to open and merge the spreadsheets using the spreadsheetmanager
        spreadSheetManager.generateCSV(aFileList, aCommonHeaderList, jobName, aHeaderRowNumber);
        //spreadSheetManager.printCollatingArray();
        //get the array of data
        String[][] dataArray = spreadSheetManager.getCollatingArray();
        //now create the subJobs using the supplied MainJob to issue the subjobs with a jobNumber
        generateSubJobsFromA2DimentionalArray(dataArray, currentJobEdit, jobName, jobNoteHeaders, otherDetailHeaders);
        return true;
    }

    /*
     **This method actually creates a list of subjobs from a given array of data
     */
    private boolean generateSubJobsFromA2DimentionalArray(String[][] dataArray, MainJob currentJobEdit, String jobName, List<String> jobNoteHeaders, List<String> otherDetailHeaders) {
    //create a variable to hold new subjobs to be added to newSubJobList
        MainJob generatedSubJob = null;
        StringBuilder subJobNotes = new StringBuilder(), subJobOtherDetails = new StringBuilder();
        //create a variable to hold a string array representing the headers of the data array
        String[] headerArray = null;
        //create variables representing the column posistions for the jobName, jobNotes and otherJobDetails        
        int jobNameHeaderColumn = 0;
        List<Integer> jobNotesColumns = new ArrayList<Integer>();
        List<Integer> otherJobDetailColumns = new ArrayList<Integer>();
        //now for everyline of the array, except the first, create a new subjob
        spreadSheetManager.printCollatingArray();
        for(int lineNumber = 0; lineNumber < dataArray.length; lineNumber++){
            //see if this is the first line of the array
            if(lineNumber == 0){
                //set the generatedSubJob to null as we are just gathering header information
                generatedSubJob = null;
            //if so store the line as the header line
             headerArray = dataArray[lineNumber];
             //now establish the positions of the headers to be mapped to the new subjobs in the dataArray
             //iterate through the headerArray
             for(int headerNumber = 0; headerNumber < headerArray.length; headerNumber++){
                 String aHeader = headerArray[headerNumber];
                 //see if aHeader is the JobName for each subJob
                 if(aHeader.equals(jobName)){
                 //if so store the header number
                     jobNameHeaderColumn = headerNumber;
                     //System.out.println("found the job title " + jobName +" in column " + headerNumber);
                 }
                 //See if the header is one that is placed in the jobNotes of the subJob
                 if(jobNoteHeaders.contains(aHeader)){
                 //If so add the headerNumber to the jobNotesColumns
                     jobNotesColumns.add(headerNumber);
                     //System.out.println("Found an item for the Job Notes: " + aHeader);
                 }
                 //See of the header is one that is placed in the otherDetails of the subJob
                 if(otherDetailHeaders.contains(aHeader)){
                 //If so add the headerNumber to the otherJobDetailColumns
                     otherJobDetailColumns.add(headerNumber);
                     //System.out.println("Found an item for the otherDetails: " + aHeader);
                 }
             }
        }
            else{
            //get the line in the dataArray being converted
            String[] dataLine = dataArray[lineNumber];
            //System.out.println("Current Line of Data is" + dataLine.toString());
            //Now Iterate through the line and create a new subjob mapping the job to the specifed jobName, jobNoteHeaders and otherDetailsHeaders            
                // create a subJob and get a jobNumber from the currentJobEdit
                    generatedSubJob = new MainJob(numberingScheme.issueSubJobNumber(currentJobEdit.getJobNumber(), currentJobEdit.getLinkedJobs().size()));
                    //Create 2 stringBuilders for the other details and jobNotes
                    subJobNotes = new StringBuilder();// now set up the string as HTML document
                    //subJobNotes.append("<html><head>");
                    subJobOtherDetails = new StringBuilder();
                    //now fill in the details of the job from the array.
            for(int headerNumber = 0; headerNumber < dataLine.length; headerNumber++){
                
                    //see if the headerNumber is jobName
                    if(jobNameHeaderColumn == headerNumber){
                        //If so set the jobName with the string at the currentpoint in the iteration
                        generatedSubJob.setName(dataLine[headerNumber]);
                        //System.out.println("Setting JobName to " + dataLine[headerNumber]);
                    }
                    //Now see if the headerNumber is in the jobNotesColumns
                    if(jobNotesColumns.contains(headerNumber) && dataLine[headerNumber] != null && (dataLine[headerNumber].equals("") == false)){
                    //if so then add the String that will be added to the subJobNotes StringBuilder
                        String jobNoteItem =  headerArray[headerNumber] + ": " + dataLine[headerNumber] + "<br>";
                      subJobNotes.append(jobNoteItem);
                    }
                    //Now see if the headerNumber is in the otherJobDetailColumns
                    if(otherJobDetailColumns.contains(headerNumber) && dataLine[headerNumber] != null && (dataLine[headerNumber].equals("") == false)){
                    //If so then add the String that will be added the subJobOtherDetails StringBuilder
                        String otherJobItem = headerArray[headerNumber] + ": " + dataLine[headerNumber] + "\n";
                       subJobOtherDetails.append(otherJobItem);
                    }
                    //now close off the HTML document for job notes
                    //subJobNotes.append("</body></html>");
                    //System.out.println("jobnote is" + subJobNotes.toString());
            generatedSubJob.setNotes(subJobNotes.toString());
            generatedSubJob.setOtherDetails(subJobOtherDetails.toString());
            
            
                }
                    //add the generatedSubJob to the new jobList
                    newSubJobList.add(generatedSubJob);
            //Now add the subJob to the newSubJobList once it's jobNotes and otherDetails have been set, unless the generatedJob is null
                         
          }
        }  
        return true;

    }

    /*private void generateSubJobsFromDeliminatedString(String aString, String deliminator) {
        String closing = "";
        
        //First split the string by the <H1> and see if we have a number of subjobs \\<.*?>"
        String[] subjobStrings = aString.split("");
        closing = "</H1>";
        System.out.println(" split string is " + subjobStrings[0]);
        if (subjobStrings.length == 1){}
        /* If there are no H1 headings then look for h2 headings
            subjobStrings = aString.split("<H2>");
            closing = "</H2>";
        }
        else{
        if (subjobStrings.length == 1){
        // If there are no H2 headings then look for h3 headings
            subjobStrings = aString.split("<H3>");
            closing = "</H3>";
        }
        else{
        if (subjobStrings.length == 1){
        // If there are no H3 headings then look for h4 headings
            subjobStrings = aString.split("<H4>");
            closing = "</H4>";
        }
        else{
        if (subjobStrings.length == 1){
        // If there are no H4 headings then look for h5 headings
            subjobStrings = aString.split("<H5>");
            closing = "</H5>";
            
        }
        else{
        if (subjobStrings.length == 1){
        // If there are no H6 headings then look no further
            //create a new subjob
          SubJob aSubJob =  new SubJob(currentJob.issueSubJobNumber());
          aSubJob.setJobName("undefiend subJob");
          aSubJob.setJobNotes(aString);
          newSubJobList.add(aSubJob);
          //as the string cannot be further parsed then force the method to return
          return;
        }
        }
        }
        }
        }*//*
                //now the string has been split into subjobs work through each string creating a subjob
        for( String aJobString : subjobStrings){
        //retrieve the job title from aJobString
            String[] splitString = aJobString.split(closing);
            System.out.println("the current String is" + aJobString);
            String jobName = splitString[0];
            String jobNotes = splitString[1];
            SubJob aSubJob =  new SubJob(currentJob.issueSubJobNumber());
            aSubJob.setJobName(jobName);
            aSubJob.setJobNotes(jobNotes);
            newSubJobList.add(aSubJob);
            if(splitString.length >2){
            System.out.println("data loss");
            }
            System.out.println("imported Job " + jobName);
        }
        
    }*/

/*
    private void parseDeliminatedFile(File afile, String deliminator) throws IOException{
      //first try to open the file and load up the string from the file
      StringBuffer fileContentString = new StringBuffer();//stringBuffer to hold the complete string from the file
        
      FileReader fileReader = new FileReader(afile);
      BufferedReader bReader = new BufferedReader(fileReader);
      String line;
      while((line = bReader.readLine()) != null){
          System.out.println("current Line is "+ line);
      fileContentString.append(line);
      }
      if (generatingSubJobs){
          System.out.println("Whole string of file is "+fileContentString);
      generateSubJobsFromDeliminatedString(fileContentString.toString(),deliminator);
      }
        */
    }

