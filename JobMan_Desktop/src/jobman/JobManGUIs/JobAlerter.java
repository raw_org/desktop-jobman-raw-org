/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package jobman.JobManGUIs;


import jobman.JobManGUIs.JobAlertDialog;
import java.awt.Point;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import jobman.JobSorter;
import JobTypes.MainJob;

/**
 *
 * @author Robert Worrall
 */
public class JobAlerter {

    private JobAlertDialog alertDialog; // this dialog shows reminders from different MainJobs or SubJobs
    private Timer jobAlertTimer;// this is a timer that controls how often the jobAlertDialog is updated.
    private ArrayList <MainJob> jobsDueNow = new ArrayList();// arraylist filled with due reminders.
    private ArrayList <MainJob> jobsDueInTheNextHour = new ArrayList();// arraylist filled with jos due in the next hour
    private JobSorter alertSorter = new JobSorter(); // This sorts the current joblist to those that are due in the next hour or those that are due now
    private Set <MainJob> mainJobList;
    public JobAlerter() {
     
        jobAlertTimer = new Timer(true);
        alertDialog = new JobAlertDialog();
        
        sortJobsDueInTheNextHourTask task1 = new sortJobsDueInTheNextHourTask();
        sortDueJobsTask task2 = new sortDueJobsTask();
        
        //start the job alert tasks
        //jobAlertTimer.scheduleAtFixedRate(task1,1000,70000);
        //jobAlertTimer.scheduleAtFixedRate(task2,1010,60000);
        
        
         
    }

    public void setMainJobList(Set<MainJob> mainJobList) {
        this.mainJobList = mainJobList;
    }

    public void dialogPosition(Point aPoint){
        alertDialog.setLocation(aPoint);
    }

    public Point getDialogPosition(){
    return alertDialog.getLocation();
    }
    
    public void showAlerts(boolean show){
        alertDialog.setVisible(show);
    }

    public void showAlerts(){
        if(alertDialog.isVisible() == true){
            alertDialog.setVisible(false);
       }
       else{
            alertDialog.setVisible(true);
            }
    }

 /*
   * Inner class that implements a TimerRask to check periodically for jobs that are due.
   * It does this by running the method sortJobsDueNow on an instance of JobSorter.
   * This forms part of a 2 part strategy to sort jobs with the minmum of processing time. 
   * This part is run once a minute where as the sortJobsDueInTheNextHourTask runs once 
   * an hour and in theory could be a very large list.
   */
   class sortDueJobsTask extends TimerTask{
       
        public void run() {
            //first get a list the Job instances that are due now (MainJobs or SubJobs)
            jobsDueNow = alertSorter.sortJobsDueNow(jobsDueInTheNextHour);
            // Add the list of these Job Instances to the JobAlertDialog instance
            alertDialog.addAlerts(jobsDueNow);
            ////System.out.println("sorted jobs due now");
        }
   
   }
   
     /*
   * Inner class that implements a TimerRask to check periodically for jobs that are within the next hour.
   * It does this by running the method sortJobsDueNow on an instance of JobSorter.
   * This forms part of a 2 part strategy to sort jobs with the minmum of processing time. 
   * This part is run once a minute where as the sortJobsDueInTheNextHourTask runs once 
   * an hour and in theory could be a very large list.
   */
      class sortJobsDueInTheNextHourTask extends TimerTask{

       
        public void run() {           
            jobsDueInTheNextHour = alertSorter.sortJobsDueInTheNextHour(new HashSet(mainJobList));
            ////System.out.println("sorted jobs due in the next hour " + mainJobList);
        }
   }
    
    
    
}
