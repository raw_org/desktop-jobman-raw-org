/*
 * JobEditorUI.java
 *
 * Created on 29 January 2007, 19:12
 * Copyright (c) <2008>, <Robert Worrall>
 *  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * Neither the name of the <ORGANIZATION> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package jobman.JobManGUIs;

import java.io.File;
import java.util.Date;
import java.text.DateFormat;
import javax.swing.JOptionPane;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import java.util.Iterator;
import javax.swing.SpinnerNumberModel;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFileChooser;
import javax.swing.event.TableModelListener;
import javax.swing.event.TableModelEvent;
import javax.swing.filechooser.FileNameExtensionFilter;
import JobTypes.MainJob;
import jobman.Reporter.JobShortReporter;
import jobman.database.jobnumberingschemes.JobNumberingSchemeInterface;
import jobman.models.CatagoryPickerDialog;


/**
 *
 * @author  Prime - Work
 * 
 * This  class is the primary means of editting any given MainJob or MainJob.
 * It uses a recursive method to calculate the total amount of time spent.  
 */
public class JobEditorUI extends javax.swing.JDialog implements TableModelListener {

    private MainJob currentJobEdit;//This is the Job being editted.
    private DateFormat dateFormater;// This is to format dates and display them appropriatly within the UI
    private SpinnerNumberModel percentageNumberModel; // This is the model for the pecentage complete. It prevents the percentage complete going over 100 and below 0
    private JobManagerUI parentWindow; //this is the window that created this instance of JobEditor. It is required to allow the save feature to work
    private ArrayList<String> catagoryList;// This is the list of catagories that current database is using
    private WPView jobNotes;// This is where the HTML formatted text of the variable jobNotes in the Job class is editted.
    private JobEditorUI superJobEditor = null;//this is the window that created this instance of JobEditor. It is required to allow the save feature to work
    private boolean jobChanged = false;// this is so the user can be informed if any changes have occured without being saved
    private boolean subjobTablechanged;// this it to ensure that changes made to a table are not missed by the tableListener
    //private boolean storeChanges;
    private boolean startStopWatchWhenVisible = false;
    private MainJob predicate;// the job to compare subjobs to
    private JobManager jobManager;
    private long timeSpentWhenOpeningJob;
    private ArrayList<MainJob> subjoblistBuffer = new ArrayList<MainJob>();// buffer for subjobs to reduce calls to the database.
    private JobNumberingSchemeInterface numberScheme;
   
    //java.awt.Frame parent
    /**Creates a new JobEditorUI and stores a reference to the JobEditorUI that created it and the JobManageUI*/
    public JobEditorUI(JobEditorUI jobEditorUI, JobManagerUI parent, boolean modal, String numberingScheme) {
        super(parent, modal);
        setNumberScheme(numberingScheme);
        parentWindow = parent; // assign the JobManagerUI to the parentWindow variable. This veriable is the instance of JobManger that is edittion this subJob/mainJob and all it's subjobs.
        dateFormater = DateFormat.getDateInstance(DateFormat.LONG);// create an instance of date formater and set it to Long
        dateFormater.setLenient(true); // set the dataformater leniency to true
        initComponents(); // initialise the GUI parts of the this instance of JobEditorUI

        percentageNumberModel = (SpinnerNumberModel) percentageSpinner.getModel(); // setup the percentage model for this instance of JobEditorUI
        subJobTable.getModel().addTableModelListener(this);// add a table listener to look for changes in the subJobTable
        jobNotes = new WPView(); // create an instance of WPView for the JobNotes variable
        jobNotes.setSize(500, 500); // set the jobNotes size to 500 by 500
        jobNotesPanel.add(jobNotes); // add it to the GUI        
        superJobEditor = jobEditorUI; // assign the JobEditorUI instance to the variable
        //swatch.start();
        // superJobEditor.setVisible(false);

    }

    
    
    
    /** Creates new form JobEditorUI and stores a reference to the JobManagerUI that created it */
    public JobEditorUI(JobManagerUI parent, boolean modal, String numberingScheme) {
        super(parent, modal);
        setNumberScheme(numberingScheme);
        parentWindow = parent;// assign the JobManagerUI to the parentWindow variable. This veriable is the instance of JobManger that is edittion this subJob/mainJob and all it's subjobs.
        dateFormater = DateFormat.getDateInstance(DateFormat.LONG);// create an instance of date formater and set it to long 
        dateFormater.setLenient(true); // set the dataformater leniency to true
        initComponents();// initialise the GUI parts of the this instance of JobEditorUI
        percentageNumberModel = (SpinnerNumberModel) percentageSpinner.getModel();// setup the percentage model for this instance of JobEditorUI
        subJobTable.getModel().addTableModelListener(this);// add a table listener to look for changes in the subJobTable
        jobNotes = new WPView();// create an instance of WPView for the JobNotes variable
        jobNotes.setSize(500, 500);// set the jobNotes size to 500 by 500
        jobNotesPanel.add(jobNotes);// assign the JobEditorUI instance to the variable

        //parent.setVisible(false);
    }

    /** Creates new form JobEditorUI and stores a reference to the window that created it */
    public JobEditorUI(java.awt.Frame parent, boolean modal, String numberingScheme) {
        super(parent, modal);
        setNumberScheme(numberingScheme);
        //parentWindow = parent;
        dateFormater = DateFormat.getDateInstance(DateFormat.LONG);// create an instance of date formater and set it to long
        dateFormater.setLenient(true);// set the dataformater leniency to true
        initComponents();// initialise the GUI parts of the this instance of JobEditorUI
        percentageNumberModel = (SpinnerNumberModel) percentageSpinner.getModel();// setup the percentage model for this instance of JobEditorUI
        subJobTable.getModel().addTableModelListener(this);// add a table listener to look for changes in the subJobTable
        jobNotes = new WPView();// create an instance of WPView for the JobNotes variable
        jobNotes.setSize(500, 500);// set the jobNotes size to 500 by 500
        jobNotesPanel.add(jobNotes);// assign the JobEditorUI instance to the variable
        //parent.setVisible(false);
    }

    public void setNumberScheme(String numberingScheme) {
        try {
            this.numberScheme = (JobNumberingSchemeInterface) Class.forName(numberingScheme).newInstance();
            
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(JobManagerUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean isJobChanged() {
        return jobChanged;
    }

    private String getSelectedJobNumberFromSubTable() {
        String result = "";
        //get the row selected from the subJobTable
        int selectedRow = subJobTable.getSelectedRow();
        // if a row is selected then find the jobNumber 
        if (selectedRow < 0) {
            // no subjob is selected return the empty string
        } else {
            // get the model of the subJobTable
            DefaultTableModel subJobTableModel = (DefaultTableModel) subJobTable.getModel();
            // pull the vector representing the table data
            Vector dataList = subJobTableModel.getDataVector();
            // pull the vector representing the selected row
            Vector rowData = (Vector) dataList.get(selectedRow);
            // pull the string representing the jobNumber
            result = (String) rowData.elementAt(2);
        }
        // return the result
        ////System.out.println("Subjob Number picked is" + result);
        return result;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jobNotesPanel = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jobNumberLabel = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        catagoryLabel = new javax.swing.JLabel();
        jobNumberValue = new javax.swing.JLabel();
        reminderLabel = new javax.swing.JLabel();
        percentageSpinner = new javax.swing.JSpinner();
        catagoryTextField = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jobNameField = new javax.swing.JTextField();
        reminderJfield = new javax.swing.JTextField();
        pickReminderDateButton = new javax.swing.JButton();
        pickCatagoryButton = new javax.swing.JButton();
        jToolBar1 = new javax.swing.JToolBar();
        saveButton = new javax.swing.JButton();
        printButton = new javax.swing.JButton();
        jobCompleteCheckBox1 = new javax.swing.JCheckBox();
        dateCreatedLabel = new javax.swing.JLabel();
        dateCreatedValue = new javax.swing.JLabel();
        dateFinishedLabel = new javax.swing.JLabel();
        dateStartedLabel = new javax.swing.JLabel();
        dateStartedValue = new javax.swing.JLabel();
        dateFinishedValue = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        subJobTable = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        addSubJobButton = new javax.swing.JButton();
        removeSubJobButton = new javax.swing.JButton();
        editSubJobButton = new javax.swing.JButton();
        importSubJobButton = new javax.swing.JButton();
        showUncompleteJobs = new javax.swing.JCheckBox();
        applyFilterToggleButton = new javax.swing.JToggleButton();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        otherDetailText = new javax.swing.JTextArea();
        jLabel9 = new javax.swing.JLabel();
        swatch2 = new stopwatch.SwatchPanel2();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        jobNotesPanel.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jobNotesPanelFocusLost(evt);
            }
        });

        javax.swing.GroupLayout jobNotesPanelLayout = new javax.swing.GroupLayout(jobNotesPanel);
        jobNotesPanel.setLayout(jobNotesPanelLayout);
        jobNotesPanelLayout.setHorizontalGroup(
            jobNotesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 455, Short.MAX_VALUE)
        );
        jobNotesPanelLayout.setVerticalGroup(
            jobNotesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 444, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 810, Short.MAX_VALUE)
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jobNumberLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jobNumberLabel.setText("Job Number");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel6.setText("Percentage Complete");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel7.setText("Reminder");

        catagoryLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        catagoryLabel.setText("Catagory");

        jobNumberValue.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jobNumberValue.setText("Value");

        reminderLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        reminderLabel.setForeground(new java.awt.Color(255, 0, 0));
        reminderLabel.setText("Value");

        percentageSpinner.setModel(        new SpinnerNumberModel(0, //initial value
            0, //min
            100, //max
            1));                //step);
percentageSpinner.addChangeListener(new javax.swing.event.ChangeListener() {
    public void stateChanged(javax.swing.event.ChangeEvent evt) {
        percentageSpinnerStateChanged(evt);
    }
    });
    percentageSpinner.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            percentageSpinnerFocusLost(evt);
        }
    });

    catagoryTextField.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            catagoryTextFieldFocusLost(evt);
        }
    });

    jLabel11.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    jLabel11.setText("Job Name");

    jobNameField.setText("Type the name of the Job here");
    jobNameField.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            jobNameFieldFocusLost(evt);
        }
    });

    reminderJfield.setText("type a new date or");
    reminderJfield.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            reminderJfieldFocusLost(evt);
        }
    });
    reminderJfield.addKeyListener(new java.awt.event.KeyAdapter() {
        public void keyPressed(java.awt.event.KeyEvent evt) {
            reminderJfieldKeyPressed(evt);
        }
    });

    pickReminderDateButton.setText("pick a date/time");
    pickReminderDateButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            pickReminderDateButtonActionPerformed(evt);
        }
    });

    pickCatagoryButton.setText("pick a catagory");
    pickCatagoryButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            pickCatagoryButtonActionPerformed(evt);
        }
    });

    saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jobman/SaveIconunpressed.jpg"))); // NOI18N
    saveButton.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
    saveButton.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/jobman/SaveIcon.jpg"))); // NOI18N
    saveButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            saveButtonActionPerformed(evt);
        }
    });
    jToolBar1.add(saveButton);

    printButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jobman/printIconunpressed.jpg"))); // NOI18N
    printButton.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
    printButton.setPressedIcon(new javax.swing.ImageIcon(getClass().getResource("/jobman/printIcon.jpg"))); // NOI18N
    printButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            printButtonActionPerformed(evt);
        }
    });
    jToolBar1.add(printButton);

    jobCompleteCheckBox1.setText("Complete");
    jobCompleteCheckBox1.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            jobCompleteCheckBox1ActionPerformed(evt);
        }
    });

    dateCreatedLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    dateCreatedLabel.setText("Date Created:");

    dateCreatedValue.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
    dateCreatedValue.setText("dateCreatedValue");

    dateFinishedLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    dateFinishedLabel.setText("Date Finished:");

    dateStartedLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    dateStartedLabel.setText("Date Started:");

    dateStartedValue.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
    dateStartedValue.setText("dateStartedValue");

    dateFinishedValue.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
    dateFinishedValue.setText("dateFinishedValue");

    subJobTable.setModel(new javax.swing.table.DefaultTableModel(
        new Object [][] {

        },
        new String [] {
            "Job Name", "Completed", "Job No"
        }
    ) {
        Class[] types = new Class [] {
            java.lang.Object.class, java.lang.Boolean.class, java.lang.Object.class
        };
        boolean[] canEdit = new boolean [] {
            true, true, false
        };

        public Class getColumnClass(int columnIndex) {
            return types [columnIndex];
        }

        public boolean isCellEditable(int rowIndex, int columnIndex) {
            return canEdit [columnIndex];
        }
    });
    subJobTable.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
    subJobTable.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            subJobTableFocusLost(evt);
        }
    });
    jScrollPane1.setViewportView(subJobTable);
    if (subJobTable.getColumnModel().getColumnCount() > 0) {
        subJobTable.getColumnModel().getColumn(1).setMinWidth(50);
        subJobTable.getColumnModel().getColumn(1).setPreferredWidth(70);
        subJobTable.getColumnModel().getColumn(1).setMaxWidth(80);
        subJobTable.getColumnModel().getColumn(2).setMinWidth(50);
        subJobTable.getColumnModel().getColumn(2).setPreferredWidth(50);
        subJobTable.getColumnModel().getColumn(2).setMaxWidth(70);
    }

    addSubJobButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    addSubJobButton.setText("Add");
    addSubJobButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            addSubJobButtonActionPerformed(evt);
        }
    });

    removeSubJobButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    removeSubJobButton.setText("Remove");
    removeSubJobButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            removeSubJobButtonActionPerformed(evt);
        }
    });

    editSubJobButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    editSubJobButton.setText("Edit");
    editSubJobButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            editSubJobButtonActionPerformed(evt);
        }
    });

    importSubJobButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    importSubJobButton.setText("Import");
    importSubJobButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            importSubJobButtonActionPerformed(evt);
        }
    });

    showUncompleteJobs.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
    showUncompleteJobs.setText("Show Uncomplete Jobs");
    showUncompleteJobs.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            showUncompleteJobsActionPerformed(evt);
        }
    });

    applyFilterToggleButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    applyFilterToggleButton.setText("Filter");
    applyFilterToggleButton.addActionListener(new java.awt.event.ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            applyFilterToggleButtonActionPerformed(evt);
        }
    });

    javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
    jPanel5.setLayout(jPanel5Layout);
    jPanel5Layout.setHorizontalGroup(
        jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel5Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jScrollPane1, 0, 0, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addComponent(showUncompleteJobs)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(applyFilterToggleButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addComponent(addSubJobButton)
                    .addGap(2, 2, 2)
                    .addComponent(editSubJobButton)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(removeSubJobButton)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(importSubJobButton)))
            .addGap(198, 198, 198)
            .addComponent(jLabel2))
    );
    jPanel5Layout.setVerticalGroup(
        jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel5Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 137, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(editSubJobButton)
                .addComponent(removeSubJobButton)
                .addComponent(importSubJobButton)
                .addComponent(addSubJobButton))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(showUncompleteJobs)
                .addComponent(applyFilterToggleButton))
            .addContainerGap(21, Short.MAX_VALUE))
    );

    javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
    jPanel2.setLayout(jPanel2Layout);
    jPanel2Layout.setHorizontalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel2Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap())
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jobNumberLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(25, 25, 25)
                                .addComponent(jobNumberValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jobNameField, javax.swing.GroupLayout.PREFERRED_SIZE, 202, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(dateStartedLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(dateStartedValue, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(dateFinishedLabel)
                                    .addComponent(dateCreatedLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                        .addComponent(dateCreatedValue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(14, 14, 14))
                                    .addComponent(dateFinishedValue, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(110, 110, 110)))
                        .addGap(123, 123, 123))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(reminderLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(173, 173, 173))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(percentageSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jobCompleteCheckBox1))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(reminderJfield, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pickReminderDateButton))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(catagoryLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(catagoryTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pickCatagoryButton, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
    );
    jPanel2Layout.setVerticalGroup(
        jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(jPanel2Layout.createSequentialGroup()
            .addContainerGap()
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jobNumberValue)
                .addComponent(jobNumberLabel))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel11)
                .addComponent(jobNameField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(dateStartedLabel)
                .addComponent(dateStartedValue))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addComponent(dateFinishedLabel)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(dateCreatedLabel))
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addComponent(dateFinishedValue)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(dateCreatedValue)))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel6)
                .addComponent(percentageSpinner, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(jobCompleteCheckBox1))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel7)
                .addComponent(reminderJfield, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(pickReminderDateButton))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(reminderLabel)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(catagoryLabel)
                .addComponent(catagoryTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addComponent(pickCatagoryButton))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
    );

    jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jobman/jobEditor.jpg"))); // NOI18N

    otherDetailText.setColumns(20);
    otherDetailText.setFont(new java.awt.Font("Monospaced", 0, 14)); // NOI18N
    otherDetailText.setRows(5);
    otherDetailText.addFocusListener(new java.awt.event.FocusAdapter() {
        public void focusLost(java.awt.event.FocusEvent evt) {
            otherDetailTextFocusLost(evt);
        }
    });
    jScrollPane3.setViewportView(otherDetailText);

    jLabel9.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
    jLabel9.setText("Other Details");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jobNotesPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel1)
                                .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE))
                            .addGap(37, 37, 37))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 448, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 308, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(swatch2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(40, 40, 40)))))
            .addContainerGap())
    );
    layout.setVerticalGroup(
        layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
        .addGroup(layout.createSequentialGroup()
            .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(16, 16, 16)
                    .addComponent(jobNotesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 494, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addComponent(jLabel9)
                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 180, Short.MAX_VALUE))
                .addGroup(layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(swatch2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addContainerGap())
    );

    pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * This method compares two Jobs and returns true if any simularties. e.g the same job name
     */
    private boolean match(MainJob currentJob, MainJob predicate) {
        boolean result = false;
        //create a collection of terms we are looking for
        Collection<String> listOfTermsToLookForInJobName;
        Collection<String> listOfTermsToLookForInOtherText;
        listOfTermsToLookForInJobName = new HashSet(Arrays.asList(predicate.getName().split(" ")));
        ////System.out.println("list Of Terms To Look For In JobName " + listOfTermsToLookForInJobName.toString());
        listOfTermsToLookForInOtherText = new HashSet(Arrays.asList(predicate.getNotes().split(" ")));
        ////System.out.println("list Of Terms To Look For In Other Text " + listOfTermsToLookForInOtherText.toString());
        String currentJobName = currentJob.getName();
        String currentJobOtherText = currentJob.getNotes();
        //put all the other text from jobNotes and otherCustomerDetails together.
        currentJobOtherText = currentJobOtherText + " " + currentJob.getOtherDetails();
        // Now see anything matches from the list of 'items to look for' and the jobName and other text in the currentJob
        //check the jobName
        //make sure the currentJobName is not an empty string if so jump this check
        if (predicate.getName().length() == 0) {
            // if true do nothing
        } else {
            for (String item : listOfTermsToLookForInJobName) {
                if (currentJobName.contains(item)) {
                    //System.out.println("currentJobName matches item " + currentJobName.contains(item) + " match is " + item.length());
                    result = true;
                    ////System.out.println("result is " + result);
                    return result;
                }// end of if
            }// end of for
        }// end of else

        // now check the otherText
        //see if the current is practically empty
        if (predicate.getNotes().length() == 0) {
            // if so do nothing
        } else {
            for (String item : listOfTermsToLookForInOtherText) {
                if (currentJobOtherText.contains(item)) {
                    //make sure the item is not a space.
                    if (item.equalsIgnoreCase(" ") || item.isEmpty()) {
                        // do not do anything
                    } else {
                        result = true;
                        //System.out.println("result is " + result);
                        return result;
                    }

                }//end of if
            }//end of for
        }//end of else
        //System.out.println("result is " + result);
        return result;
    }

    /*
    This method deals with enter being pressed when the focus is on the reminderJfield JTextBox
     */
    private void reminderJfieldKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_reminderJfieldKeyPressed
        // If Enter is pressed
        if (evt.getKeyCode() == evt.VK_ENTER) {
            //create a new variable for a Date instance
            Date newReminderDate;
            //create a new string and get the text from the reminderJField
            String newDateString = reminderJfield.getText();
            // Ensure the dateFormatter is set to short
            dateFormater = DateFormat.getDateInstance(DateFormat.SHORT);
            //Now see if we can parse the string of text from reminderJfield into a date
            try {
                // parse the string to a new instance of Date
                newReminderDate = dateFormater.parse(newDateString);
                //set the reminderLable to the Date instance, once it has been formattted by the dateformatter so that is is suitable to be displayed in the GUI
                reminderLabel.setText(dateFormater.format(newReminderDate) + " " + (DateFormat.getTimeInstance().format(newReminderDate)));
                //set the reminder variable to the newly parse Date instance
                currentJobEdit.setReminder(newReminderDate);
                // set the jobChanged variable to true to ensure that the changes are not unintentionally lost
                jobChanged = true;
            } //If the String newDateString cannot be parsed catch the parse exception and try to parse with a medium format
            catch (ParseException e1) {
                //    ////System.out.println(e1 + " trying other formats");
                //Set the date format to medium
                dateFormater = DateFormat.getDateInstance(DateFormat.MEDIUM);
                // now try to parse the string newDateString again
                try {
                    //parse the string
                    newReminderDate = dateFormater.parse(newDateString);
                    //set the reminderLable to the Date instance, once it has been formattted by the dateformatter so that is is suitable to be displayed in the GUI
                    reminderLabel.setText(dateFormater.format(newReminderDate) + " " + (DateFormat.getTimeInstance().format(newReminderDate)));
                    //set the reminder variable to the newly parse Date instance
                    currentJobEdit.setReminder(newReminderDate);
                    // set the jobChanged variable to true to ensure that the changes are not unintentionally lost
                    jobChanged = true;
                    ////System.out.println("parsing successful");
                } //If the String newDateString cannot be parsed catch the parse exception and try to parse with a medium format
                catch (ParseException e2) {
                    //   ////System.out.println(e2 + " trying other formats");
                    //Set the date format to long
                    dateFormater = DateFormat.getDateInstance(DateFormat.LONG);
                    // now try to parse the string newDateString again
                    try {
                        //parse the string
                        newReminderDate = dateFormater.parse(newDateString);
                        //set the reminderLable to the Date instance, once it has been formattted by the dateformatter so that is is suitable to be displayed in the GUI
                        reminderLabel.setText(dateFormater.format(newReminderDate) + " " + (DateFormat.getTimeInstance().format(newReminderDate)));
                        //set the reminder variable to the newly parse Date instance
                        currentJobEdit.setReminder(newReminderDate);
                        // set the jobChanged variable to true to ensure that the changes are not unintentionally lost
                        jobChanged = true;
                        //////System.out.println("parsing successful");
                    } //If the String newDateString cannot be parsed catch the parse exception
                    catch (ParseException e3) {
                        //   ////System.out.println(e3 + " trying other formats");
                    }
                }
            }
            // set the dateFormater to short before the method exits
            dateFormater = DateFormat.getDateInstance(DateFormat.SHORT);
        } else {
            ////System.out.println("key pressed" + evt.getKeyCode() + "key wanted is" + evt.VK_ENTER);
        }
    }//GEN-LAST:event_reminderJfieldKeyPressed

    /*
     * This method prints a summary of the current Job being edited
     */
    private void printButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_printButtonActionPerformed
        // First Create an instance of ShortJobReporter
        JobShortReporter reporter = new JobShortReporter(jobManager);
        // generate a string report with reporter and the currentJobEdit
        String report = reporter.generateShortReport(currentJobEdit);
        // create an instance of WPView
        WPView printer = new WPView();
        //assign it the String generated by the reporter
        printer.setText(report);
        // now print the document using WPViews print method
        printer.print();
    }//GEN-LAST:event_printButtonActionPerformed

    /*
     * This method checks that when the current JobEditorUI is being closed that Job has not been changed
     * If so, ask the user if they want to save the changes. The method also checks to see if the stopwatch is running and stops it.
     */
    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing

        //First see if the stopwatch is running by checking its status
        boolean swatchRunning = swatch2.isRunning();// this is so that if the stopwatch is running it can be stopped to retrive the updated time spent.
        if (swatchRunning) {
            // if the stopwatch is running then stop it
            swatch2.stopSWatch();
        }
        // Now check to see if the jobChanged variable is set to true or that the currentJobEdits timeSpent variable is not equal that set in the stopwatch.
        if (jobChanged == true || currentJobEdit.getTimeSpent() != swatch2.getAllottedTime()) {
            //if either of the above results in true then the job has changed and the user needs to be asked if they want to save the changes
            Object[] options = {"Yes", "No"};
            int n = JOptionPane.showOptionDialog(this, "The text items in this job have changed do you wish to save them?", "Job has changed", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
            if (n == 0) {
                //If the user has requested to save the changes run the method saveChanges
                saveChanges();
                // and stop the stopwatchs thread of execution
                //swatch.stop();
            }
            if (n == 1) {
                // if the user has elected to not save the changes then stop the stopwatches thread and exit
                if (swatchRunning) {
                    swatch2.startSWatch();
                }
            }


        }


        if (superJobEditor == null) {
            //parentWindow.setVisible(true);
        } else {
            //superJobEditor.setVisible(true);
        }
        //Stop the stopwatches thread of execution
//    swatch.stop();
    }//GEN-LAST:event_formWindowClosing

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed

        saveChanges();
    }//GEN-LAST:event_saveButtonActionPerformed

    private void jobNotesPanelFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jobNotesPanelFocusLost

        jobChanged = true;
    }//GEN-LAST:event_jobNotesPanelFocusLost
    /*
     * This method removes a selected subJob from the from the linkJobList variable in the currentJobEdit
     * and from the subJobTable
     */
    private void removeSubJobButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeSubJobButtonActionPerformed


        int rowNumber;// the variable to hold which row is selected at present.
        MainJob subJob = null; // the subjob that is selected
        rowNumber = subJobTable.getSelectedRow();// get the row number of the subjob selected in the subJobtable
        //////System.out.println("row selected is " + rowNumber);
        //see if no row is selected
        if (rowNumber < 0) {
            // If no row is selected then do nothing
        } else {
            // check with the user that these subjobs should be deleted.
            int confirmResult = JOptionPane.showConfirmDialog(
                    parentWindow,
                    "Do you really want to delete this/ these Jobs?",
                    "Confirm Deletion",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.WARNING_MESSAGE);
            if (confirmResult == 0) {
                //if a row is selected then selected subJob from the linkedJob variable in the currentJobEdit
                String currentMainJobNumber = currentJobEdit.getLinkedJobs().get(rowNumber);
                //if the subjob is in the buffer retrieve it from here and not from the database
                if(subjoblistBuffer.isEmpty() == false){
                //create an iterator for the buffer
                    Iterator <MainJob>jobitorator =  subjoblistBuffer.iterator();
                    while(jobitorator.hasNext()){
                        MainJob aJob = jobitorator.next();
                    if (aJob.getJobNumber().equals(currentMainJobNumber)){
                    subJob = aJob;
                    }
                    }
                }
                else{
                    subJob = jobManager.findMainJobByJobNumber(currentMainJobNumber);
                     }
                
                if(subJob != null){
                // remove the subJob from the linkedJob variable
                currentJobEdit.getLinkedJobs().remove(currentMainJobNumber);
                // remove the subjob from the database.    
                jobManager.deleteJob(subJob);
                //remove the job from the subjoblistbuffer
                subjoblistBuffer.remove(subJob);
                }
                
                // set the jobChanged variable to true to ensure that the changes to the current job edit are not unintentially lost.
                jobChanged = true;
                // refersh the subJobTable
                refreshJobTable();
            }
        }

    }//GEN-LAST:event_removeSubJobButtonActionPerformed
    /*
     * This method creates an instance of CatagoryPickerDialog which the user can create or pick a catagory
     */
    private void pickCatagoryButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pickCatagoryButtonActionPerformed

        int[] pickedList;// This is an array to hold the choices picked by the user
        //create an instance of CatagoryPickerDialog and assign it the list of current catagories
        CatagoryPickerDialog catagoryPicker = new CatagoryPickerDialog(parentWindow, true, catagoryList);
        //make the CatagoryPickerDialog instance visible
        catagoryPicker.setVisible(true);
        //get what the user picked
        pickedList = catagoryPicker.getPickedCatagories();
        // clear the current value of the variable catagory in the currentJobEdit
        currentJobEdit.setCategory("");
        // create the string to hold the catagory for the currentJobEdit
        String catagoryPicked = "";
        //iterate through the list of catagories picked so that one string contains all of the catagories picked
        int i;
        for (i = 0; i < pickedList.length; i++) {
            catagoryPicked = catagoryPicked + " " + catagoryList.get(pickedList[i]);
        }
        //assign the new string to the catagory variable in the currentJobEdit
        catagoryTextField.setText(catagoryPicked);
        // set the jobChanged variable to true to ensure that the changes to the current job edit are not unintentially lost.
        jobChanged = true;
    }//GEN-LAST:event_pickCatagoryButtonActionPerformed

    private void catagoryTextFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_catagoryTextFieldFocusLost

        // set the jobChanged variable to true to ensure that the changes to the current job edit are not unintentially lost.
        jobChanged = true;

    }//GEN-LAST:event_catagoryTextFieldFocusLost
    /*
     * This method opens a new instance of JobEditorUI to edit a selected MainJob in
     * the subJobTable
     */
    private void editSubJobButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editSubJobButtonActionPerformed

        int rowNumber;// the row that is selected in the subJobTable
        String subJobNumber;// the subjob that is selected
        boolean swatchRunning = swatch2.isRunning();//states if the stopwatch is running when the subjob is being openned for editting.
        MainJob subJob = null;// variable that contains the subjob to be editted
        //establish if the stopwatch is running.
        if (swatchRunning) {
            //if the stopwatch is running stop it, update the timeSpent variable on currentJobEdit and set the jobChanged variable to true.
            swatch2.stopSWatch();
            // update the timeSpent variable in the currentJobEdit
            timeSpentResetCheck();
            currentJobEdit.incrementTimeSpent(getTimeSpentAtThisLevel());
            timeSpentWhenOpeningJob = swatch2.getAllottedTime();
            // set the jobChanged variable to true to ensure that the changes to the current job edit are not unintentially lost.
            //inform the user that the current job needs to be saved before editing the subjob if the job has changed.
        }
        if (jobChanged == false || jobChangedContinue()) {
            //check the time has not been reset
            timeSpentResetCheck();
            //store the currentJobEdit to the database
            saveChanges();
            //now lets edit the subjob
            // create a JobEditorUI to edit the subjob with.
            JobEditorUI subJobEditor = new JobEditorUI(this, parentWindow, true, numberScheme.getClass().getCanonicalName());
            //get the row selected in the subJobTable
            rowNumber = subJobTable.getSelectedRow();
            //get the subJobNumber
            subJobNumber = getSelectedJobNumberFromSubTable();

            //////System.out.println("row selected is " + rowNumber);
            if (rowNumber < 0) {// if no row is selected in the subjobTable then do nothing
            } else {
                // get the subJob by the subjob Name
                if (subJobNumber.equals("")) {
                    // No Job has been selected and do nothing
                } else {
                    //iterate through the list of linkedMainJobs and set a matching subJobNumber to the subJob variable

                    ArrayList<String> subJobList = currentJobEdit.getLinkedJobs();
                    for (String aMainJob : subJobList) {
                        if (aMainJob.equals(subJobNumber)) {
                            // //System.out.println("subJob jobNumber " + aMainJob.getJobNumber()+ " equals " + subJobNumber);
                            subJob = jobManager.findMainJobByJobNumber(aMainJob);
                            break;
                        } else {
                            subJob = null;
                        }
                    }
                }

                // get the Subjob to be edited by the row number in the subjob table
                //subJob = currentJobEdit.getLinkedJobs().get(rowNumber);
                // get the Subjob to be edited by the jobnumber
                //check that subJob is not null if so do nothing otherwise open the job for editing
                if (subJob != null) {
                    // set the jobEditorUI we just created the MainJob to be editted
                    subJobEditor.setCurrentJobEdit(subJob, jobManager);
                    //see if the stopwatch is running. if so start the stopwatch on the subjobEditor
                    if (swatchRunning) {
                        subJobEditor.startSwatch();
                    }
                    ////System.out.println("Editing Job" + subJob.getJobNumber());
                    // lets edit the job
                    subJobEditor.editJob();
                    //save the subjob
                    jobManager.storeJob(subJob);

                    //update the stopwatch.
                    swatch2.setAllottedTime(jobManager.getTimeSpentOnJob(currentJobEdit.getJobNumber()));
                    //update the variable
                    timeSpentWhenOpeningJob = swatch2.getAllottedTime();
                    // set the jobChanged variable to true to ensure that the changes to the current job edit are not unintentially lost.
                    jobChanged = true;
                    //update the subJobTable
                    refreshJobTable();
                }
            }
        }

        //if the swatch was running when we opened the subjob for editting then restart the stopwatch
        if (swatchRunning) {
            swatch2.startSWatch();
        }

    }//GEN-LAST:event_editSubJobButtonActionPerformed

    private void otherDetailTextFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_otherDetailTextFocusLost

        // set the jobChanged variable to true to ensure that the changes to the current job edit are not unintentially lost.
        jobChanged = true;

    }//GEN-LAST:event_otherDetailTextFocusLost
    /*
     * This opens up an instance of DateAndTimePicker and gets the date picked by the user and sets it to the reminderDate variable in the currentJobEdit
     */
    private void pickReminderDateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pickReminderDateButtonActionPerformed

        //Create an instyance of DateAndTimePicker
        DateAndTimePicker reminderPicker = new DateAndTimePicker(parentWindow, true);
        // create a variable to hold the picked date by the user
        Date newReminderDate;
        //open the DateAndTimePicker instance and get the picked date
        newReminderDate = reminderPicker.getPickedTimeAndDate();
        //set the reminderDate variable in the currentJobEdit
        currentJobEdit.setReminder(newReminderDate);
        // Set the dateFormater to short format
        dateFormater = DateFormat.getDateInstance(DateFormat.SHORT);
        //assign the reminerLable to a string formatted by the dateFormater using the newly picked Date.
        reminderLabel.setText(dateFormater.format(newReminderDate) + " " + (DateFormat.getTimeInstance().format(newReminderDate)));
        // set the jobChanged variable to true to ensure that the changes to the current job edit are not unintentially lost.
        jobChanged = true;
    }//GEN-LAST:event_pickReminderDateButtonActionPerformed
    /*
     * This method takes a string from the reminder Field and parses it to a date instance
     */
    private void reminderJfieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_reminderJfieldFocusLost
        //redundant code
     /*   Date newReminderDate;
        String newDateString = reminderJfield.getText();
        try{
        newReminderDate = dateFormater.parse(newDateString);
        reminderLabel.setText(dateFormater.format(newReminderDate));
        currentJobEdit.setReminder(newReminderDate);
        // set the jobChanged variable to true to ensure that the changes to the current job edit are not unintentially lost.
        jobChanged = true;
        }
        catch (ParseException e1){
        //    ////System.out.println(e1 + " trying other formats");
        dateFormater = DateFormat.getDateInstance(DateFormat.MEDIUM);

        try{
        newReminderDate = dateFormater.parse(newDateString);
        reminderLabel.setText(dateFormater.format(newReminderDate));
        currentJobEdit.setReminder(newReminderDate);
        // set the jobChanged variable to true to ensure that the changes to the current job edit are not unintentially lost.
        jobChanged = true;
        //////System.out.println("parsing successful");
        }
        catch (ParseException e2){
        //   ////System.out.println(e2 + " trying other formats");
        dateFormater = DateFormat.getDateInstance(DateFormat.LONG);

        try{
        newReminderDate = dateFormater.parse(newDateString);
        reminderLabel.setText(dateFormater.format(newReminderDate));
        currentJobEdit.setReminder(newReminderDate);
        // set the jobChanged variable to true to ensure that the changes to the current job edit are not unintentially lost.
        jobChanged = true;
        //////System.out.println("parsing successful");
        }
        catch (ParseException e3){
        //   ////System.out.println(e3 + " trying other formats");
        
        }    
        }    
        }
        dateFormater = DateFormat.getDateInstance(DateFormat.SHORT);*/
    }//GEN-LAST:event_reminderJfieldFocusLost

    private void percentageSpinnerFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_percentageSpinnerFocusLost

        // set the jobChanged variable to true to ensure that the changes to the current job edit are not unintentially lost.
        jobChanged = true;
        if ((percentageNumberModel.getNumber().intValue() == currentJobEdit.getPercentComplete()) && currentJobEdit.getPercentComplete() == 0) {//see if the job is different to the currentValue and just being started
        }
    }//GEN-LAST:event_percentageSpinnerFocusLost

    private void jobNameFieldFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jobNameFieldFocusLost

        // set the jobChanged variable to true to ensure that the changes to the current job edit are not unintentially lost. 
        jobChanged = true;
        //////System.out.println("the job name has ben set to" + currentJobEdit.getJobName());
    }//GEN-LAST:event_jobNameFieldFocusLost
    /*
     * This method adds a MainJob to the linkJobs variable on the currentJobEdit
     */
    private void addSubJobButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addSubJobButtonActionPerformed


        //get the name of the subjob from the user.
        String jobName = JOptionPane.showInputDialog(parentWindow, "Please Name The New MainJob");
        if (jobName != null) {
            //Create a subjob using the currentJobEdit to issue a jobNumber
            MainJob aMainJob = new MainJob(numberScheme.issueSubJobNumber(currentJobEdit.getJobNumber(), currentJobEdit.getLinkedJobs().size()));
            aMainJob.setName(jobName);
            currentJobEdit.getLinkedJobs().add(aMainJob.getJobNumber());
            //store the new subJob into the database
            jobManager.storeJob(aMainJob);
            // set the jobChanged variable to true to ensure that the changes to the current job edit are not unintentially lost.
            //save the currentJobEdit becuase it the subjob could be lost
            jobManager.storeJob(currentJobEdit);
            //put the subjob into the subJobBuffer
            subjoblistBuffer.add(aMainJob);
            jobChanged = false;
            // update the subJobTable to reflect the changes in the currentJobEdit
            refreshJobTable();
        }
        // add the new subJob to the currentJobEdits linkedJobs variable

    }//GEN-LAST:event_addSubJobButtonActionPerformed
    /**This checks to see if the table has changed and tableChanged listener has missed it */
    private void subJobTableFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_subJobTableFocusLost

        subjobTablechanged = false;

    }//GEN-LAST:event_subJobTableFocusLost

private void importSubJobButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_importSubJobButtonActionPerformed
    
    JobImporter importer;
        try {
            importer = new JobImporter(numberScheme.getClass().getCanonicalName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(JobEditorUI.class.getName()).log(Level.SEVERE, null, ex);
            return;
        }
    File[] files;
    // create a file filter for comma deliminated files
    FileNameExtensionFilter filter = new FileNameExtensionFilter("Comma Delininated SpreadSheet", "csv");
    // create a new JFileChooser to pick the files we wish to import as jobs
    JFileChooser chooser = new JFileChooser();
    //add the filter to the JFileChooser
    chooser.addChoosableFileFilter(filter);
    //set the title of the JFileChooser
    chooser.setDialogTitle("Pick The Files You Wish to Import");
    //set the JFileChooser to accept multiple files
    chooser.setMultiSelectionEnabled(true);
    int returnVal = chooser.showOpenDialog(this);

    if (returnVal == JFileChooser.APPROVE_OPTION) {
        //get the selected files
        files = chooser.getSelectedFiles();
        //ask the user what row the headers reside in
        String s = (String) JOptionPane.showInputDialog("What row does the header information reside?");
        int rowNumber = Integer.parseInt(s);
        //Now get the list of headers from the spreadsheet

        List<String> allHeaders = importer.getAllHeaders(files, rowNumber);
        //If no Headers are returned then exit the method
        if (allHeaders == null) {
            return;
        }
        //pick the common headers
        List<String> commonHeaderList;
        //create a commonHeaderPicker dialog box and load it up with headers
        CommonHeaderPicker commonHeaderPicker = new CommonHeaderPicker(parentWindow, true);
        commonHeaderPicker.setHeaderList(allHeaders);
        //show the dialog box
        commonHeaderPicker.setVisible(true);
        // now get a list of common headers
        commonHeaderList = commonHeaderPicker.getCommonHeaders();
        //if the commonHeaderList is empty or null then exit the method
        if (commonHeaderList == null || commonHeaderList.isEmpty()) {
            return;
        }
        // Now map the headers to the Job structure
        VisualJobMapper visualJobMapper = new VisualJobMapper(parentWindow, true);
        visualJobMapper.setCommonHeaders(commonHeaderList);
        visualJobMapper.setDataList(allHeaders);
        //now open the visual mapper for editting
        visualJobMapper.setVisible(true);
        //now get an array representing all the jobs
        //get the header representing the jobname
        String commonJobName = visualJobMapper.getJobName();
        List<String> jobNoteHeaders = visualJobMapper.getJobNoteHeaders();
        List<String> otherJobDetailsHeaders = visualJobMapper.getOtherDetailsHeaders();
        if (commonJobName == null || jobNoteHeaders == null || jobNoteHeaders == null || otherJobDetailsHeaders == null) {
            return;
        }
         boolean success = false;
        //convert the array of files into a list of files for the importer method
        List<File> aFileList = Arrays.asList(files);
        success = importer.importSubjobsFromCVS(aFileList, commonHeaderList, commonJobName, jobNoteHeaders, otherJobDetailsHeaders, rowNumber, currentJobEdit);
        List<MainJob> subJobList = importer.getNewSubJobList();
        //Store all the new Subjobs into the database
        for (MainJob aJob : subJobList) {
            jobManager.storeJob(aJob);
            currentJobEdit.getLinkedJobs().add(aJob.getJobNumber());
            //store the subjob to the buffer
            subjoblistBuffer.add(aJob);
        }

        refreshJobTable();
        if (success) {
            ////System.out.println("successfully imported jobs");
            JOptionPane.showMessageDialog(parentWindow, "JobManager has imported " + subJobList.size() + " Jobs");
        } else {
            ////System.out.println("Failure to import jobs");
        }
    }


}//GEN-LAST:event_importSubJobButtonActionPerformed

private void jobCompleteCheckBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jobCompleteCheckBox1ActionPerformed
    if (jobCompleteCheckBox1.isSelected()) {
        currentJobEdit.setPercentComplete(100);
        currentJobEdit.setFinishDate(new Date());
        percentageSpinner.setValue(100);
        jobChanged = true;
    } else {
        currentJobEdit.setPercentComplete(0);
        currentJobEdit.setFinishDate(null);
        percentageSpinner.setValue(0);
    }
}//GEN-LAST:event_jobCompleteCheckBox1ActionPerformed

private void percentageSpinnerStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_percentageSpinnerStateChanged
    // TODO add your handling code here:
}//GEN-LAST:event_percentageSpinnerStateChanged

private void showUncompleteJobsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showUncompleteJobsActionPerformed
    refreshJobTable();
}//GEN-LAST:event_showUncompleteJobsActionPerformed

private void applyFilterToggleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_applyFilterToggleButtonActionPerformed
    if (applyFilterToggleButton.isSelected()) {

        JobFilterDialog filterDialog = new JobFilterDialog(parentWindow, true);
        filterDialog.setVisible(true);
        predicate = filterDialog.getPredicate();
        if (predicate == null) {
            applyFilterToggleButton.setSelected(false);
        }
    } else {
        predicate = null;
    }
    refreshJobTable();
}//GEN-LAST:event_applyFilterToggleButtonActionPerformed
    /**
     * @param args the command line arguments
     */
    /*    public static void main(String args[]) {
    java.awt.EventQueue.invokeLater(new Runnable() {
    public void run() {
    new JobEditorUI(new javax.swing.JFrame(), true).setVisible(true);
    }
    });
    }*/
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addSubJobButton;
    private javax.swing.JToggleButton applyFilterToggleButton;
    private javax.swing.JLabel catagoryLabel;
    private javax.swing.JTextField catagoryTextField;
    private javax.swing.JLabel dateCreatedLabel;
    private javax.swing.JLabel dateCreatedValue;
    private javax.swing.JLabel dateFinishedLabel;
    private javax.swing.JLabel dateFinishedValue;
    private javax.swing.JLabel dateStartedLabel;
    private javax.swing.JLabel dateStartedValue;
    private javax.swing.JButton editSubJobButton;
    private javax.swing.JButton importSubJobButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JCheckBox jobCompleteCheckBox1;
    private javax.swing.JTextField jobNameField;
    private javax.swing.JPanel jobNotesPanel;
    private javax.swing.JLabel jobNumberLabel;
    private javax.swing.JLabel jobNumberValue;
    private javax.swing.JTextArea otherDetailText;
    private javax.swing.JSpinner percentageSpinner;
    private javax.swing.JButton pickCatagoryButton;
    private javax.swing.JButton pickReminderDateButton;
    private javax.swing.JButton printButton;
    private javax.swing.JTextField reminderJfield;
    private javax.swing.JLabel reminderLabel;
    private javax.swing.JButton removeSubJobButton;
    private javax.swing.JButton saveButton;
    private javax.swing.JCheckBox showUncompleteJobs;
    private javax.swing.JTable subJobTable;
    private stopwatch.SwatchPanel2 swatch2;
    // End of variables declaration//GEN-END:variables

    /*
     * This method is ususally invoked before the Instance is displayed
     */
    public void setCurrentJobEdit(MainJob aJob, JobManager aJobManager) {
        //Set the currentJobEdit variable to aJob
        currentJobEdit = aJob;
        //Set the catagoryList to aCatagoryList
        catagoryList = aJobManager.getCatagoryList();
        //set the jobManager
        jobManager = aJobManager;
        //Set the title of the JobEditorUI window to the currentJobEdit jobName variable 
        setTitle(currentJobEdit.getName());
        //see the number of subjobs and if the jobdepth is 5 then disable the subjob table preventing subjobs below 5 from being created a lost
       /* if(subJobDepth() == 5){ //no longer required since bete 5
        subJobTable.setEnabled(false);
        addSubJobButton.setEnabled(false);
        removeSubJobButton.setEnabled(false);
        showUncompleteJobs.setEnabled(false);
        editSubJobButton.setEnabled(false);
        importSubJobButton.setEnabled(false);
        }*/
    }

    /*
     * This method opens the currentJobEdit to be editted by the user
     */
    public void editJob() {
        //connect up the data first
        connectDataModel();
        // now make it visible
        setVisible(true);
    }

    /*
     * This method connects the cuttentJobEdit to the GUI ready for editting
     */
    private void connectDataModel() {
        List<String> aListofSubJobs;// a list of the subjobs linked to this job
        DefaultTableModel subJobTableModel;//used to populate the subJobTable with data
        Vector<Object> subJobValues;// this is for the tableModel for the subJobTable
        //set the label denoting the job number. 
        jobNumberValue.setText(currentJobEdit.getJobNumber().toString());
        // set the label denoting the date the job was created.
        dateCreatedValue.setText(dateFormater.format(currentJobEdit.getCreationDate()));
        //check to see if the Job has been started yet
        if (currentJobEdit.getstartDate() == null) {
            // if the job has not been started set the set the label denoting the date job was started to "Job Not Started"
            dateStartedValue.setText("Job Not Started");
        } // otherwise set the label denoting the date job was started
        else {
            dateStartedValue.setText(dateFormater.format(currentJobEdit.getstartDate()));
        }
        //Set the label for date finished if the value of the dateFinished variable on the currentJobEdit is not set to null
        if (currentJobEdit.getFinshDate() == null) {
            //if null set the label to an Job Not Complete
            dateFinishedValue.setText("Job Not Complete");
        } else {
            // otherwise set the dateFinishedValue JLable to the date Finished
            dateFinishedValue.setText(dateFormater.format(currentJobEdit.getFinshDate()));
        }

        // set the label denoting the jobName
        jobNameField.setText(currentJobEdit.getName());
        // set the value denoting the percentage a job is complete
        percentageSpinner.setValue(currentJobEdit.getPercentComplete());
        //See if a reminder date has been picked for this Job
        if (currentJobEdit.getReminder() == null) {
            //If a reminderDate has not been selected then set the label denoting the reminder
            reminderLabel.setText("No Date Set");
        } else {
            //otherwise set the label denoting the reminder date
            reminderLabel.setText(dateFormater.format(currentJobEdit.getReminder()) + " " + (DateFormat.getTimeInstance().format(currentJobEdit.getReminder())));
        }
        // set the label denoting the catagory
        catagoryTextField.setText(currentJobEdit.getCatagory());
        // set the textField denoting any other details
        otherDetailText.setText(currentJobEdit.getOtherDetails());
        //setup the subJob table by first getting the tableModel
        subJobTableModel = (DefaultTableModel) subJobTable.getModel();
        //now  get a list of subJobs from the currentJobEdit
        aListofSubJobs = currentJobEdit.getLinkedJobs();
        // create an iterator to go through the list of sub Jobs
        Iterator<String> subJobIterator = aListofSubJobs.iterator();
        //now iterate through the list of sub Jobs adding any to the suBjobTable and buffering the job in subJobListBuffer
        String subJobNumber;
        while (subJobIterator.hasNext()) {
            //get the current subjob to be added to the subjobtable
            subJobNumber = subJobIterator.next();
            MainJob currentMainJob = jobManager.findMainJobByJobNumber(subJobNumber);
            //add the subjob to the subJobBuffer
            subjoblistBuffer.add(currentMainJob);
            //create a Vector instance to hold the values to be represented in the table
            subJobValues = new Vector<Object>();
            //add the name of the subJob
            subJobValues.add(currentMainJob.getName());
            // add true or false to the vector denoting if the subjob is complete.
            if (currentMainJob.getPercentComplete() == 100) {
                subJobValues.add(true);
            } else {
                subJobValues.add(false);
            }
            // Add the MainJobs name the to Vector
            subJobValues.add(currentMainJob.getJobNumber());
            // add the Vector to the model of the subJobTable
            subJobTableModel.addRow(subJobValues);
        }
        //connect up jobNotes
        jobNotes.setText(currentJobEdit.getNotes());
        //connect the stopwatch
        swatch2.setAllottedTime(jobManager.getTimeSpentOnJob(currentJobEdit.getJobNumber()));
        // get the time spent when opening the job
        timeSpentWhenOpeningJob = swatch2.getAllottedTime();
        //setup the jobCompleteCheckbox
        if (currentJobEdit.getPercentComplete() == 100) {
            jobCompleteCheckBox1.setSelected(true);
        }

    }
    /*
     * This method updates the subjobTable by deleting the data in the table's 
     * model and reinputting the data from the currentJobEdits linkedJobs variable
     */

    private void refreshJobTable() {
        List<String> subJobList;// An arraylist of MainJobs that are to be placed in the table
        DefaultTableModel subJobTableModel; // the table model of the subJobTable
        Vector<Object> subJobValues; // A Vector of values represnting a given MainJob in the subJobTable
        Vector<Vector> jobDataValues; // A Vector of Vectors representing the rows of MainJobs in the subJobTable
        // Get the existing table model from the subJobTable
        subJobTableModel = (DefaultTableModel) subJobTable.getModel();
        //clear the current table;
        jobDataValues = (Vector) subJobTableModel.getDataVector();
        jobDataValues.clear();
        //now add the subJobs
        //first get a the list of MainJobs from the MainJobTable 
        //see if there are jobs in the buffer if not then retrieve the jobs and put them into the buffer as
        if (subjoblistBuffer.isEmpty()) {
            subJobList = currentJobEdit.getLinkedJobs();
            //see if the jobs should be filtered to show uncomplete jobs

            // Create an Interator to interate through the list of MainJobs
            Iterator<String> subJobIterator = subJobList.iterator();
            // Go through the list of MainJobs add each that is found to the subJobTable
            String subJobNumber;
            while (subJobIterator.hasNext()) {
                subJobNumber = subJobIterator.next();
                // get a subJob
                MainJob currentMainJob = jobManager.findMainJobByJobNumber(subJobNumber);
                subjoblistBuffer.add(currentMainJob);
                // see if the show if the 'Show Uncomplete Jobs' tick box has been pressed if so do not include this subJob
                if (showUncompleteJobs.isSelected() && currentMainJob.getPercentComplete() >= 100) {
                    // if this is the case do not add this job
                } else {
                    //see if there is a predicate i.e check if the predicate variable is null and the currentMainJob does not match the predicate
                    if (predicate != null && match(currentMainJob, predicate) == false) {
                        // if so do not add the job to the model
                        ////System.out.println("non matching job");
                    } else {
                        // Create a Vector instance to hold the values represent in the MainJob
                        subJobValues = new Vector<Object>();
                        // Add the MainJobs name the to Vector
                        subJobValues.add(currentMainJob.getName());
                        // Now add true or false to the Vector denoting if the MainJob is Complete or not
                        if (currentMainJob.getPercentComplete() == 100) {
                            // If the percentComplete varable on the MainJob is a 100 then true is added to the Vector
                            subJobValues.add(true);
                        } else {
                            // If the percentComplete varable on the MainJob is anything else than 100 then place false in to the Vector
                            subJobValues.add(false);
                        }
                        //now add the jobNumber of the subjob
                        subJobValues.add(currentMainJob.getJobNumber());
                        //Add the Vector to the Vactor representing the data in the Model
                        jobDataValues.add(subJobValues);

                    }

                }
            }
        }// there are jobs in the subjobbuffer so use the buffer
        else {
            Iterator<MainJob> jobIterator = subjoblistBuffer.iterator();
            while (jobIterator.hasNext()) {
                MainJob currentMainJob = jobIterator.next();

                // see if the show if the 'Show Uncomplete Jobs' tick box has been pressed if so do not include this subJob
                if (showUncompleteJobs.isSelected() && currentMainJob.getPercentComplete() >= 100) {
                    // if this is the case do not add this job
                } else {
                    //see if there is a predicate i.e check if the predicate variable is null and the currentMainJob does not match the predicate
                    if (predicate != null && match(currentMainJob, predicate) == false) {
                        // if so do not add the job to the model
                        ////System.out.println("non matching job");
                    } else {
                        // Create a Vector instance to hold the values represent in the MainJob
                        subJobValues = new Vector<Object>();
                        // Add the MainJobs name the to Vector
                        subJobValues.add(currentMainJob.getName());
                        // Now add true or false to the Vector denoting if the MainJob is Complete or not
                        if (currentMainJob.getPercentComplete() == 100) {
                            // If the percentComplete varable on the MainJob is a 100 then true is added to the Vector
                            subJobValues.add(true);
                        } else {
                            // If the percentComplete varable on the MainJob is anything else than 100 then place false in to the Vector
                            subJobValues.add(false);
                        }
                        //now add the jobNumber of the subjob
                        subJobValues.add(currentMainJob.getJobNumber());
                        //Add the Vector to the Vactor representing the data in the Model
                        jobDataValues.add(subJobValues);

                    }

                }

            }
        }

        // Now that any MainJobs have been represented in the table model update the GUI so that is displays the changes
        subJobTableModel.fireTableDataChanged();
    }

    /*
     * This method handles changes made to a given MainJob through the subJobTable.
     */
    public void tableChanged(TableModelEvent e) {
        List<String> subJobList; // A list of MainJobs aquired from the currentJobEdit
        DefaultTableModel subJobTableModel; // the table model for the subJobTable
        //First get the model of the subJobTable
        subJobTableModel = (DefaultTableModel) subJobTable.getModel();
        // Now establish where in the table the change has taken place
        // get the row of change
        int row = e.getFirstRow();
        // get the column of the change
        int column = e.getColumn();
        //////System.out.println("column " +column);
        //////System.out.println("row " +row);
        // Get the model from the source of the event triggering this method
        subJobTableModel = (DefaultTableModel) e.getSource();
        // Now get the new data from the tabel model 

        //If Column 0 is selected then the MainJobs name has changed and store to the corresponding MainJob
        if (column == 0) {
            // get the new MainJob name
            String newJobName = (String) subJobTableModel.getValueAt(row, column);
            // Now get the list of linked Jobs from the currentJobEdit
            subJobList = currentJobEdit.getLinkedJobs();
            // Get the corresponding MainJob from the list of subJobs using the row variable
            //MainJob aMainJob = subJobList.get(row);
            MainJob aMainJob = null;
            //get the subJobName from the row that is selected
            String subJobNumber = getSelectedJobNumberFromSubTable();
            // get the subJob by the subjob Name
            if (subJobNumber.equals("")) {
                // No Job has been selected and do nothing
            } else {
                //see if there are any buufered jobs if there are do not get the job from the database
                if (subjoblistBuffer.isEmpty()) {
                    //iterate through the list of linkedMainJobs and set a matching subJobNumber to the subJob variable
                    subJobList = currentJobEdit.getLinkedJobs();
                    for (String subJob : subJobList) {
                        if (subJob.equals(subJobNumber)) {
                            aMainJob = jobManager.findMainJobByJobNumber(subJob);
                            break;
                        }
                    }
                } else {// use the subjobBuffer to find the mainjob.
                    for (MainJob subJob : subjoblistBuffer) {
                        if (subJob.getJobNumber().equals(subJobNumber)) {
                            aMainJob = subJob;
                            break;
                        }
                    }
                }

            }
            //see if the subJob is null
            if (aMainJob != null) {
                // set the jobName variable to it's new value
                aMainJob.setName(newJobName);
                //now store the updated job
                jobManager.storeJob(aMainJob);
            }


        } else {
            // If Column 1 is selected then the MainJobs percentageComplete has changed to either 0 ot 100, ie false or true respectivly
            if (column == 1) {
                //establish the boolean set in the table model
                Boolean jobComplete = (Boolean) subJobTableModel.getValueAt(row, column);
                // Now get the list of linked Jobs from the currentJobEdit
                subJobList = currentJobEdit.getLinkedJobs();
                // Get the corresponding MainJob from the list of subJobs using the row variable
                //MainJob aMainJob = subJobList.get(row);

                MainJob aMainJob = null;
                //get the subJobName from the row that is selected
                String subJobNumber = getSelectedJobNumberFromSubTable();
                // get the subJob by the subjob Name
                if (subJobNumber.equals("")) {
                    // No Job has been selected and do nothing
                } else {
                                    //see if there are any buufered jobs if there are do not get the job from the database
                if (subjoblistBuffer.isEmpty()) {
                    //iterate through the list of linkedMainJobs and set a matching subJobNumber to the subJob variable
                    subJobList = currentJobEdit.getLinkedJobs();
                    for (String subJob : subJobList) {
                        if (subJob.equals(subJobNumber)) {
                            aMainJob = jobManager.findMainJobByJobNumber(subJob);
                            break;
                        }
                    }
                } else {// use the subjobBuffer to find the mainjob.
                    for (MainJob subJob : subjoblistBuffer) {
                        if (subJob.getJobNumber().equals(subJobNumber)) {
                            aMainJob = subJob;
                            break;
                        }
                    }
                }

                }

                if (aMainJob != null) {
                    // Set the percentComplete variable in the MainJob to either 100 or 0 depending of the boolean value retreaved from the table model
                    if (jobComplete.equals(true)) {
                        // The boolean is true then set the percentComplete to 100
                        aMainJob.setPercentComplete(100);
                        //set the finish date for the job.
                        aMainJob.setFinishDate(new Date());
                        //////System.out.println("subJob Completed");
                        //set the jobStarted to todays date if null
                        if (aMainJob.getstartDate() == null) {
                            aMainJob.setstartDate(new Date());
                        }
                    }
                    if (jobComplete.equals(false)) {
                        //The boolean value is false then set the percentComplete to 0
                        aMainJob.setPercentComplete(0);
                        //set the finish date to null
                        aMainJob.setFinishDate(null);
                        //////System.out.println("subJob Not Completed");
                    }
                    //now store the updated job
                    jobManager.storeJob(aMainJob);
                }
            } else {
                // no recongisable colomn has been altered
            }
            // set the jobChanged variable to true to ensure that the changes to the current job edit are not unintentially lost.
            jobChanged = true;
        }
    }

    public void startSwatch() {
        startStopWatchWhenVisible = true;
    }


    /*
     * Method that saves the components of a job by getting the values from the 
     * GUI form and setting them into the currentJobEdit
     */
    private void saveChanges() {
        //See if the stopwatch is running and assign it to a variable
        boolean swatchRunning = swatch2.isRunning();
        //If the stopwatch is running, stop it.
        if (swatchRunning) {
            //Stopping the stopwatch
            swatch2.stopSWatch();
            //see if the currentJobEdits time has been reset.

        } else {
        }
        timeSpentResetCheck();
        //Update the currentJobEdit timeSpent variable.
        currentJobEdit.incrementTimeSpent(getTimeSpentAtThisLevel());
        timeSpentWhenOpeningJob = swatch2.getAllottedTime();
        //Update the Text components of currentJobEdit
        currentJobEdit.setNotes(jobNotes.getText());
        //System.out.println("Job notes are: " + currentJobEdit.getJobNotes());
        currentJobEdit.setCategory(catagoryTextField.getText());
        currentJobEdit.setOtherDetails(otherDetailText.getText());
        currentJobEdit.setCategory(catagoryTextField.getText());
        currentJobEdit.setPercentComplete(percentageNumberModel.getNumber().intValue());
        // if the percentage complete field is 100 then set the finshDate variable to todays date
        if (currentJobEdit.getPercentComplete() == 100 && currentJobEdit.getFinshDate() == null) {
            currentJobEdit.setFinishDate(new Date());
        } //otherwise check the job has not become uncomplete if so set the finsh date variable to null
        else {
            if (currentJobEdit.getPercentComplete() < 100 & currentJobEdit.getFinshDate() != null) {
                currentJobEdit.setFinishDate(null);
            }
        }
        currentJobEdit.setName(jobNameField.getText());
        //update the title of the window
        this.setTitle(currentJobEdit.getName());
        //currentJobEdit.incrementTimeSpent(swatch2.getAllottedTime());
        // if the date started is null and the timeSpent is greater than 0 then get todays date and set it to startDate variable in the job being editted 
        if (currentJobEdit.getstartDate() == null && currentJobEdit.getTimeSpent() > 0) {
            currentJobEdit.setstartDate(new Date());
            // now set the dateStartedValue lable to the start date using the dateFormater
            dateStartedValue.setText(dateFormater.format(currentJobEdit.getstartDate()));
        }
        //See if there is a parent window denoting if the currentJobEdit can be saved
        if (parentWindow != null) {
            // There is a parent Window and therefore the Updated Job can be stored
            //parentWindow.storeCurrentJobEdit();// no longer needed now that the jobManager is a singleton
            jobManager.storeJob(currentJobEdit);
            // set the jobChanged variable to false now that the changes to the current job edit cannot be unintentially lost.
            jobChanged = false;
        } else {
            jobManager.storeJob(currentJobEdit);
        }
        // if the stopwatch was running when this method was started then restart it
        if (swatchRunning) {
            //restarting the stopwatch.
            swatch2.startSWatch();
        }

    }

    public void setVisible(boolean visible) {
        if (startStopWatchWhenVisible) {
            //swatch.start();
            swatch2.startSWatch();
        }
        super.setVisible(visible);
    }

    private MainJob retreiveMainJob(String jobNumber) {
        ArrayList<String> subJobList = currentJobEdit.getLinkedJobs();
        MainJob result = null;
        for (String aMainJob : subJobList) {
            if (aMainJob.equals(jobNumber)) {
                result = jobManager.findMainJobByJobNumber(aMainJob);
            }
        }
        return result;
    }

    private boolean jobChangedContinue() {
        boolean result = false;

        Object[] options = {"Yes", "No"};
        int n = JOptionPane.showOptionDialog(this, "Some items in this job have changed and need to be saved before continuing do you wish to continue?", "Job has changed", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        if (n == 0) {
            //If the user has requested to save the changes run the method saveChanges
            return true;

        }
        if (n == 1) {
            return false;
        }

        return result;
    }

    private void timeSpentResetCheck() {
        //System.out.println("checking reset time");
        //see if the time spent is less than when the job was first opened.
        if (timeSpentWhenOpeningJob > swatch2.getAllottedTime()) {
            //System.out.println("reseting time spent");
            jobManager.resetTimeSpentOnJob(currentJobEdit.getJobNumber());
            currentJobEdit.incrementTimeSpent(swatch2.getAllottedTime());
            //swatch2.setAllottedTime(0);
            timeSpentWhenOpeningJob = swatch2.getAllottedTime();
            refreshJobTable();

        }
    }

    private long getTimeSpentAtThisLevel() {
        if (swatch2.getAllottedTime() == 0) {
            return 0;
        } else {
            long newTimeSpent = currentJobEdit.getTimeSpent() + (swatch2.getAllottedTime() - timeSpentWhenOpeningJob);
            return newTimeSpent;
        }


    }
    /*
    private int subJobDepth() {
    int result = 1;
    String jNumber = currentJobEdit.getJobNumber();
    if (jNumber.contains("-")){
    //count the number of - in the jNumber using regex
    Pattern pattern = Pattern.compile("-");
    Matcher matcher = pattern.matcher(jNumber);
    while(matcher.find()){
    result++;
    }
    ////System.out.println("jobDepth is " + result);
    return result;
    }
    else {
    return result;
    }
    } */
}
