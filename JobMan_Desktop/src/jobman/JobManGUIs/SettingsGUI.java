/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobman.JobManGUIs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;


/**
 *
 * @author worralr
 */
public class SettingsGUI {

    OpenDBdialog openDBdialog;

    public SettingsGUI() {
        openDBdialog = new OpenDBdialog(new javax.swing.JFrame(), true);
    }
    
    
    
    public Properties getSystemDefaultFile() {
        Properties systemDefaults = new Properties();
        //File defaultDBfile;
        //try to open the  file that contains the default settings and load the properties to systemDefaults 
        try {
            systemDefaults.load(new FileInputStream("JobManDefSet.jms"));
            // now verifi that the default settings are valid by running the method verifiSystemDefaults.
            verifySystemDefaults(systemDefaults);
        } //if unsuccessful try to create a new file to hold default settings
        catch (IOException errorA) {
            System.out.println(errorA.getMessage());
            try {
                //try to create the file
                new FileOutputStream("JobManDefSet.jms");
                //set the default values ready for storing them later to the JobManDefSet.jms file
                verifySystemDefaults(systemDefaults);
            } //Report the error if the file creation fails.
            catch (IOException errorB) {
                System.out.println(errorB.getMessage());
            }
        }
        return systemDefaults;
    }

    /**
     * A method to verify the settings in the default file
     */
    private void verifySystemDefaults(Properties systemDefaults) {
        //System.out.println("Varifing default settings");
        // check to see if the value defaultDataBase is set to null
        if (systemDefaults.getProperty("defaultDataBase") == null) {
            //System.out.println("resetting defaultDataBase");
            //if so set the property to none
            systemDefaults.setProperty("defaultDataBase", "none");
        } //otherwise do not do anything else with the defaultDatabase property
        else {
            //System.out.println("defaultDataBase is set to " + systemDefaults.getProperty("defaultDataBase"));
        }
        //Check the default posistions for the windows starting with the JobManagerUI Instance
        if (systemDefaults.getProperty("JobMangerUIPositionX") == null) {
            systemDefaults.setProperty("JobMangerUIPositionX", "0");
        }
        if (systemDefaults.getProperty("JobMangerUIPositionY") == null) {
            systemDefaults.setProperty("JobMangerUIPositionY", "0");
        }
        //check for default position for jobAlertDialogBox
        if (systemDefaults.getProperty("alertDialogX") == null) {
            systemDefaults.setProperty("alertDialogX", "0");
        }
        if (systemDefaults.getProperty("alertDialogY") == null) {
            systemDefaults.setProperty("alertDialogY", "0");
        }
        //check for default position for jobEditorUI
        if (systemDefaults.getProperty("jobEditorUIX") == null) {
            systemDefaults.setProperty("jobEditorUIX", "0");
        }
        if (systemDefaults.getProperty("jobEditorUIY") == null) {
            systemDefaults.setProperty("jobEditorUIY", "0");
        }
    }

    public File createNewDataBase() {
        File newFile = null;
        Object[] options = {"Yes",
            "No"};
        JFrame frame = new JFrame();
        int n = JOptionPane.showOptionDialog(frame,
                "To Create Another Database You Must Close This One"
                + "\n Do You Want To Continue?",
                "Create DataBase Question",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[1]);
        if (n == 0) {
            //System.out.println("Opening another Database");
            JFileChooser fileChooser;
            FileNameExtensionFilter filter;
            fileChooser = new JFileChooser();
            filter = new FileNameExtensionFilter("Job Manager Database Files", "jmdb");
            fileChooser.addChoosableFileFilter(filter);
            int returnVal = fileChooser.showSaveDialog(frame);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                newFile = fileChooser.getSelectedFile();
                if (newFile.getName().indexOf('.') == -1) {
                    newFile = new File(newFile.getAbsolutePath() + ".jmdb");

                } else {
                    //    throw new RuntimeException("System exiting because no default DB has been selected");
                }

            }

        }
        return newFile;
    }
    
    public File openJobDataBase() { 
        File openFile = null;
    JFrame frame = new JFrame();

        Object[] options = {"Yes",
            "No"};

        int n = JOptionPane.showOptionDialog(frame,
                "To Open Another Database You Must Close This One"
                + "\n Do You Want To Continue?",
                "Open DataBase Question",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[1]);
        if (n == 0) {
            //System.out.println("Opening another Database");

           
            JFileChooser fileChooser;
            FileNameExtensionFilter filter;
            fileChooser = new JFileChooser();
            filter = new FileNameExtensionFilter("Job Manager Database Files", "jmdb");
            fileChooser.addChoosableFileFilter(filter);
            int returnVal = fileChooser.showOpenDialog(frame);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                openFile = fileChooser.getSelectedFile();
                
                return openFile;
                
            }
            if (n == 1) {
            }
        }
        else {}
            return openFile;
    }

    public String checkDefaultDatabase(Properties systemDefaults) {
        String defaultDBlocation = "";
        
        File openFile;
                if (systemDefaults.getProperty("defaultDataBase").equals("none")) {
            //If none is set then request the user to create or open an existing database by creating an instance of OpenDBdialog
            
            //Now get the database file created or existing file opened by the openDBdialog
            openFile = openDBdialog.getOpenFile();
                    System.out.println("File selected is" + openFile);
            //See if the file is null
            if (openFile == null) {
                //throw a RuntimeException if this is the case.
                throw new RuntimeException("System exiting because no default DB has been selected");
            } else {
                //otherwise get the loction of the default database and set the defaultDatabase property in jobManager.systemDefaults
                defaultDBlocation = openFile.getPath();
                systemDefaults.setProperty("defaultDataBase", defaultDBlocation);
                //esablish if the file provided is a new database or an existing database. This is so the application knows if it needs to create a new database.
                if (openDBdialog.fileNewOrExisting().equals("new")) {
                    systemDefaults.setProperty("numberingScheme", "jobman.database.jobnumberingschemes.SimpleJobNumberingScheme");
                }
                if (openDBdialog.fileNewOrExisting().equals("existing")) {
                    if(systemDefaults.getProperty("numberingScheme")== null)
                        systemDefaults.setProperty("numberingScheme", "jobman.database.jobnumberingschemes.SimpleJobNumberingScheme");
                }
            }
        } // If a default file has been set then get it's location.
        else {
            defaultDBlocation = systemDefaults.getProperty("defaultDataBase");
            systemDefaults.setProperty("numberingScheme", "jobman.database.jobnumberingschemes.SimpleJobNumberingScheme");
        }               
                return defaultDBlocation;
    }

    boolean newDataBase() {
       return openDBdialog.fileNewOrExisting().equals("new");
    }

}
