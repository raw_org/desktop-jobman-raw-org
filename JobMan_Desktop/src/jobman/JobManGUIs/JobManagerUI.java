/*
 * JobManagerUI.java
 *
 * Created on 17 October 2006, 00:56
 * Copyright (c) <2008>, <Robert Worrall>
 *  All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

 * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 * Neither the name of the <R-A-W.org> nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
package jobman.JobManGUIs;

/**
 *
 * @author  Prime - Work
 */
import JobTypes.BaseTypes.Job;
import java.awt.HeadlessException;
import java.awt.Point;
import java.io.IOException;
import java.io.FileOutputStream;
import java.io.File;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.table.DefaultTableModel;
import java.util.Vector;
import java.util.Iterator;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelListener;
import javax.swing.event.TableModelEvent;
import javax.swing.ButtonGroup;
import javax.swing.JOptionPane;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import javax.swing.JFrame;
import javax.swing.table.TableColumn;

import jobman.JobException;
import JobTypes.MainJob;
import JobTypes.BaseTypes.SpawnableJob;
import jobman.Reporter.JobReporter;
import jobman.database.DataBaseCreationOrOpeningException;
import jobman.database.jobnumberingschemes.JobNumberingSchemeInterface;


public class JobManagerUI extends javax.swing.JFrame implements TableModelListener {
    // Variables for the class
    // where the jobmanager gets all it's 'MainJob' classes from

    private ArrayList<Job> mainJobList;// a list of main jobs that are represented in the main job table
    private MainJob selectedMainJob;// current mainJob Selected in the table
    private boolean finishedJobView = false; // this is whether the Main job table should have completed or uncompleted jobs in it.
    private final Timer tableRefreshTimer = new Timer(true);
    private JobManager jobManager;
    private RefreshTableTask task3 = new RefreshTableTask();
    private final JobAlerter jobAlerter = new JobAlerter();
    private JobNumberingSchemeInterface numberScheme;

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new JobManagerUI().setVisible(true);
            }
        });
    }

    /** Creates new form JobManagerUI */
    public JobManagerUI() {
        try {
            jobManager = new JobManager();
            setNumberScheme("jobman.database.jobnumberingschemes.SimpleJobNumberingScheme");
        } catch (DataBaseCreationOrOpeningException ex) {
            Logger.getLogger(JobManagerUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        //try to load the look and feel
        try {
            UIManager.setLookAndFeel("org.jvnet.substance.skin.SubstanceRavenLookAndFeel");
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        //set the frame title
        setTitle("Job Manager");

        //run the method initComponents. This is a generated method, created by the Netbeans IDE
        initComponents();
        //Run the method initComponents2
        initComponents2();
        //set the window position of jobManger
        //get the posisions from systemDefaults
        Point aPoint = new Point();
                aPoint.x = new Integer(jobManager.systemDefaults.getProperty("alertDialogX"));
        aPoint.y = new Integer(jobManager.systemDefaults.getProperty("alertDialogY"));
        jobAlerter.dialogPosition(aPoint);

        //open the default file containing all the default settings for Jobmanger

        Integer JobMangerUIPositionX = new Integer(jobManager.systemDefaults.getProperty("JobMangerUIPositionX"));//if value not set set to 0
        ////System.out.println("position of x is " + JobMangerUIPositionX);
        Integer JobMangerUIPositionY = new Integer(jobManager.systemDefaults.getProperty("JobMangerUIPositionY", "0"));//if value not set set to 0
        ////System.out.println("position of y is " + JobMangerUIPositionY);
        setLocation(JobMangerUIPositionX, JobMangerUIPositionY);//set the posistion of the window.
        //open up the defult database

        Set<MainJob> aSet = jobManager.getAllCurrentMainJobs();
        mainJobList = new ArrayList<>(aSet);
        sortMainJobList();
        refreshMainJobTable();
        subJobTable.getModel().addTableModelListener(this);
        mainJobTable.getModel().addTableModelListener(this);
        tableRefreshTimer.scheduleAtFixedRate(task3, 1020, 50000);
        







    }

    
    
    
    /** This method is called from within the constructor to
     * initialise the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */

    public void setNumberScheme(String numberingScheme) {
        try {
            this.numberScheme = (JobNumberingSchemeInterface) Class.forName(numberingScheme).newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(JobManagerUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        mainJobListing = new javax.swing.JPanel();
        jobManMainPanel = new javax.swing.JPanel();
        subJobListing = new javax.swing.JPanel();
        addSubJobButton = new javax.swing.JButton();
        removeSubJobButton = new javax.swing.JButton();
        subJobLabel = new javax.swing.JLabel();
        subJobScrollPlane = new javax.swing.JScrollPane();
        subJobTable = new javax.swing.JTable();
        editSubJobButton = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        quickSearchButton = new javax.swing.JButton();
        quickSearchTextField = new javax.swing.JTextField();
        mainJobScrollPlane = new javax.swing.JScrollPane();
        mainJobTable = new javax.swing.JTable();
        mainJobLabel = new javax.swing.JLabel();
        addMainJobButton = new javax.swing.JButton();
        removeMainJobButton = new javax.swing.JButton();
        editMainJobButton = new javax.swing.JButton();
        generateReportButton = new javax.swing.JButton();
        viewAlertsButton = new javax.swing.JButton();
        jobManagerTitleLabel = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        openJobDatabaseItem = new javax.swing.JMenuItem();
        createANewJobDatabaseItem = new javax.swing.JMenuItem();
        viewMenu = new javax.swing.JMenu();
        showCompleteRadioButton = new javax.swing.JRadioButtonMenuItem();
        showUncompleteRadioButton = new javax.swing.JRadioButtonMenuItem();
        toolsMenu = new javax.swing.JMenu();
        replicationItem = new javax.swing.JMenuItem();
        openJobAlertMenuItem = new javax.swing.JMenuItem();

        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        org.jdesktop.layout.GroupLayout mainJobListingLayout = new org.jdesktop.layout.GroupLayout(mainJobListing);
        mainJobListing.setLayout(mainJobListingLayout);
        mainJobListingLayout.setHorizontalGroup(
            mainJobListingLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 0, Short.MAX_VALUE)
        );
        mainJobListingLayout.setVerticalGroup(
            mainJobListingLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 0, Short.MAX_VALUE)
        );

        addSubJobButton.setText("Add Sub Job");
        addSubJobButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addSubJobButtonActionPerformed(evt);
            }
        });

        removeSubJobButton.setText("Remove Sub Job");
        removeSubJobButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeSubJobButtonActionPerformed(evt);
            }
        });

        subJobLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jobman/subJobs.jpg"))); // NOI18N

        subJobScrollPlane.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        subJobTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Job Name", "Job Complete"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Boolean.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        subJobScrollPlane.setViewportView(subJobTable);
        subJobTable.getColumnModel().getColumn(1).setMinWidth(30);
        subJobTable.getColumnModel().getColumn(1).setPreferredWidth(90);
        subJobTable.getColumnModel().getColumn(1).setMaxWidth(90);

        editSubJobButton.setText("Edit Sub Job");
        editSubJobButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editSubJobButtonActionPerformed(evt);
            }
        });

        quickSearchButton.setText("Quick Search");
        quickSearchButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                quickSearchButtonActionPerformed(evt);
            }
        });

        quickSearchTextField.setText("Type What You Are Seaching For Here");

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(quickSearchButton)
                .add(10, 10, 10)
                .add(quickSearchTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 249, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(quickSearchTextField, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 33, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(quickSearchButton))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout subJobListingLayout = new org.jdesktop.layout.GroupLayout(subJobListing);
        subJobListing.setLayout(subJobListingLayout);
        subJobListingLayout.setHorizontalGroup(
            subJobListingLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(subJobListingLayout.createSequentialGroup()
                .addContainerGap()
                .add(subJobListingLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(subJobLabel)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, subJobListingLayout.createSequentialGroup()
                        .add(addSubJobButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 117, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(editSubJobButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 92, Short.MAX_VALUE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(removeSubJobButton, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 128, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(subJobScrollPlane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 356, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .add(subJobListingLayout.createSequentialGroup()
                .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(3, 3, 3))
        );
        subJobListingLayout.setVerticalGroup(
            subJobListingLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(subJobListingLayout.createSequentialGroup()
                .addContainerGap()
                .add(subJobLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(subJobScrollPlane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 234, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(subJobListingLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(editSubJobButton)
                    .add(removeSubJobButton)
                    .add(addSubJobButton))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout jobManMainPanelLayout = new org.jdesktop.layout.GroupLayout(jobManMainPanel);
        jobManMainPanel.setLayout(jobManMainPanelLayout);
        jobManMainPanelLayout.setHorizontalGroup(
            jobManMainPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jobManMainPanelLayout.createSequentialGroup()
                .add(10, 10, 10)
                .add(subJobListing, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jobManMainPanelLayout.setVerticalGroup(
            jobManMainPanelLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jobManMainPanelLayout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(subJobListing, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
        );

        mainJobTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Job Number", "Job Name", "%", "Due Date"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        mainJobScrollPlane.setViewportView(mainJobTable);
        mainJobTable.getColumnModel().getColumn(0).setMinWidth(70);
        mainJobTable.getColumnModel().getColumn(0).setPreferredWidth(70);
        mainJobTable.getColumnModel().getColumn(0).setMaxWidth(200);
        mainJobTable.getColumnModel().getColumn(2).setMinWidth(30);
        mainJobTable.getColumnModel().getColumn(2).setPreferredWidth(30);
        mainJobTable.getColumnModel().getColumn(2).setMaxWidth(50);

        addMainJobButton.setText("Add New Main Job");
        addMainJobButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addMainJobButtonActionPerformed(evt);
            }
        });

        removeMainJobButton.setText("Remove Main Job");
        removeMainJobButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeMainJobButtonActionPerformed(evt);
            }
        });

        editMainJobButton.setText("Edit Job");
        editMainJobButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editMainJobButtonActionPerformed(evt);
            }
        });

        generateReportButton.setText("Generate Report");
        generateReportButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                generateReportButtonActionPerformed(evt);
            }
        });

        viewAlertsButton.setText("View Alerts");
        viewAlertsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                viewAlertsButtonActionPerformed(evt);
            }
        });

        jobManagerTitleLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jobManagerTitleLabel.setIcon(new javax.swing.ImageIcon(getClass().getResource("/jobman/jobEditor.jpg"))); // NOI18N

        fileMenu.setText("File");
        fileMenu.setFont(new java.awt.Font("Tahoma", 0, 14));

        openJobDatabaseItem.setFont(new java.awt.Font("Tahoma", 0, 14));
        openJobDatabaseItem.setText("Open Job Database");
        openJobDatabaseItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openJobDatabaseItemActionPerformed(evt);
            }
        });
        fileMenu.add(openJobDatabaseItem);

        createANewJobDatabaseItem.setFont(new java.awt.Font("Tahoma", 0, 14));
        createANewJobDatabaseItem.setText("Create A New Job Database");
        createANewJobDatabaseItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createANewJobDatabaseItemActionPerformed(evt);
            }
        });
        fileMenu.add(createANewJobDatabaseItem);

        jMenuBar1.add(fileMenu);

        viewMenu.setText("View");
        viewMenu.setFont(new java.awt.Font("Tahoma", 0, 14));

        showCompleteRadioButton.setFont(new java.awt.Font("Tahoma", 0, 14));
        showCompleteRadioButton.setText("Complete Jobs");
        showCompleteRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showCompleteRadioButtonActionPerformed(evt);
            }
        });
        viewMenu.add(showCompleteRadioButton);

        showUncompleteRadioButton.setFont(new java.awt.Font("Tahoma", 0, 14));
        showUncompleteRadioButton.setSelected(true);
        showUncompleteRadioButton.setText("Show Uncomplete Jobs");
        showUncompleteRadioButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                showUncompleteRadioButtonActionPerformed(evt);
            }
        });
        viewMenu.add(showUncompleteRadioButton);

        jMenuBar1.add(viewMenu);

        toolsMenu.setText("Tools");
        toolsMenu.setFont(new java.awt.Font("Tahoma", 0, 14));

        replicationItem.setFont(new java.awt.Font("Tahoma", 0, 14));
        replicationItem.setText("Open Job Replication Dialog");
        replicationItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                replicationItemActionPerformed(evt);
            }
        });
        toolsMenu.add(replicationItem);

        openJobAlertMenuItem.setFont(new java.awt.Font("Tahoma", 0, 14));
        openJobAlertMenuItem.setText("Open Job Alert Dialog");
        openJobAlertMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openJobAlertMenuItemActionPerformed(evt);
            }
        });
        toolsMenu.add(openJobAlertMenuItem);

        jMenuBar1.add(toolsMenu);

        setJMenuBar(jMenuBar1);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(mainJobLabel)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(mainJobScrollPlane, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 554, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(jobManMainPanel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 393, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(mainJobListing, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(jobManagerTitleLabel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, editMainJobButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, addMainJobButton, 0, 0, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, removeMainJobButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(viewAlertsButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(generateReportButton, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jobManagerTitleLabel)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(addMainJobButton)
                            .add(viewAlertsButton))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(removeMainJobButton)
                            .add(generateReportButton))))
                .add(2, 2, 2)
                .add(editMainJobButton)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(89, 89, 89)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(mainJobListing, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                                .add(mainJobLabel)
                                .add(6, 6, 6))))
                    .add(jobManMainPanel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(mainJobScrollPlane, 0, 0, Short.MAX_VALUE))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void replicationItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_replicationItemActionPerformed
       // jobManager.openReplicationDialog();
        JobReplicator jobReplicator = new JobReplicator(new JFrame(), true);// create a job replicator
        // hand jobReplicator the list of replicatableJobs.
        Set<SpawnableJob> replicatableJobList = jobManager.getReplicatableJobs();
        jobReplicator.setReplicatableJobs(replicatableJobList);
        jobReplicator.setCurrentJobList(jobManager.getAllCurrentMainJobs());
        jobReplicator.setVisible(true);//set it visable
        ArrayList<SpawnableJob> deleteJobArrayList = jobReplicator.getDeleteReplicatableJobs();//now get the list of replicatableJobs that need deleting
        //remove the deleted replicatableJobs from the database.
        Iterator<SpawnableJob> jobIterator = deleteJobArrayList.iterator();
        while (jobIterator.hasNext()) {
            jobManager.deleteReplicableJob(jobIterator.next());
        }
        ArrayList<SpawnableJob> newJobArrayList = jobReplicator.getNewReplicatableJobs();//now get the new list of replicatableJobs
        Iterator<SpawnableJob> newJobIterator = newJobArrayList.iterator();
        while (newJobIterator.hasNext()) {
            jobManager.storeReplicatableJob(newJobIterator.next());
        }

    }//GEN-LAST:event_replicationItemActionPerformed

    private void viewAlertsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_viewAlertsButtonActionPerformed
        jobAlerter.showAlerts();
    }//GEN-LAST:event_viewAlertsButtonActionPerformed

    private void generateReportButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_generateReportButtonActionPerformed
        JobReporter reporter = new JobReporter();
        JobReportViewer reportView = new JobReportViewer(this, true);
        if (selectedMainJob == null) {
        } else {
            //disable the jobManagerUI until report has finished
            ////System.out.println("setting Report");
            setEnabled(false);
            boolean buildingReportSuccessful;
            try {
                // create a tempory file to place the report into.
                File tempfile = File.createTempFile("tempReport.txt", ".tmp");
                buildingReportSuccessful = reporter.gernerateFullReport(selectedMainJob, tempfile, jobManager);
                if (buildingReportSuccessful) {
                    //System.out.println("Building of report successful");
                    //now try to open up the tempary file so that we can read the report
                    //see if the file is larger than the allocated heap size of 25600000
                    if (tempfile.length() >= 25600000) {
                        giveOptiontoSaveAsReportIsLarge(tempfile);
                    } else {
                        buildingReportSuccessful = reportView.openFile(tempfile);

                    }
                }
            } catch (IOException ex) {
                //System.out.println("Error building report " + ex);
                Logger.getLogger(JobManagerUI.class.getName()).log(Level.SEVERE, null, ex);
                buildingReportSuccessful = false;
            }
            //attributedReport = reporter.gernerateFullReport(selectedMainJob);
            ////System.out.println(attributedReport);
            //reportView.setText(attributedReport);
            setEnabled(true);
            // if the report was built and loaded then open the report viewer.
            if (buildingReportSuccessful) {
                reportView.setVisible(true);
            }

        }
        
    }//GEN-LAST:event_generateReportButtonActionPerformed

    private boolean giveOptiontoSaveAsReportIsLarge(File tempfile) throws IOException, HeadlessException {
        // if so ask if the user wants to abandon the report or save a copy to the desktop
        Object[] options = {"Save Report", "Abandon report"};
        int chosenOption = JOptionPane.showOptionDialog(this, "This report is larger than 250m <br> do you want to", "Job has changed", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[1]);
        //see which option the user has picked
        if (chosenOption == 0) {
            //If the user has requested to save the report
            //create a new filechooser
            JFileChooser chooser = new JFileChooser();
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Web Page", "html");
            chooser.addChoosableFileFilter(filter);
            //open the chooser
            int returnVal = chooser.showSaveDialog(this);
            //see if a file was chosen or to exit
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File newFile = new File(chooser.getSelectedFile().getPath() + ".html");
                if (newFile == null) {
                    //see  if the chosen file is null
                    //System.out.println("chosen file is null");
                    setEnabled(true);
                    return true;
                } else {
                    writeReportToFile(newFile, tempfile);
                }
            } else {
                //enable the jobmanger window
                setEnabled(true);
                //exit the method
                return true;
            }
        } else {
            //System.out.println("Building of report not successful");
            setEnabled(true);
            return true;
        }
        return false;
    }

    private void writeReportToFile(File report, File tempfile) throws IOException{
                                     //open filereader and writer to transfer the data from the tempfile to the new file
                                    FileWriter writer = new FileWriter(report);
                                    FileReader reader = new FileReader(tempfile);
                                    //create a buffered reader to buffer the characters
                                    BufferedReader bReader = new BufferedReader(reader);
                                    BufferedWriter bWriter = new BufferedWriter(writer);
                                    //create a char to feed from one stream to the other.
                                    int character;
                                    //feed the characters across the streams
                                    while ((character = bReader.read()) != -1) {
                                        bWriter.write(character);
                                    }
                                    //close the streams down
                                    bWriter.close();
                                    writer.close();
                                    bReader.close();
                                    reader.close();
                                    JOptionPane.showMessageDialog(this, "File saved successfully!");
    }
    
    private void showUncompleteRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showUncompleteRadioButtonActionPerformed

        finishedJobView = false;
        selectedMainJob = null;
        refreshMainJobList();
        refreshMainJobTable();
        refreshSubJobTable();
    }//GEN-LAST:event_showUncompleteRadioButtonActionPerformed

    private void showCompleteRadioButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_showCompleteRadioButtonActionPerformed

        finishedJobView = true;
        selectedMainJob = null;
        refreshMainJobList();
        refreshMainJobTable();
        refreshSubJobTable();
    }//GEN-LAST:event_showCompleteRadioButtonActionPerformed

    private void createANewJobDatabaseItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createANewJobDatabaseItemActionPerformed

        try {
            jobManager.createANewJobDatabase();
            refreshMainJobList();
        refreshMainJobTable();
        refreshSubJobTable();
        } catch (DataBaseCreationOrOpeningException ex) {
            Logger.getLogger(JobManagerUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }//GEN-LAST:event_createANewJobDatabaseItemActionPerformed

    private void openJobDatabaseItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openJobDatabaseItemActionPerformed

        try {
            jobManager.openJobDataBase();
            refreshMainJobList();
        refreshMainJobTable();
        refreshSubJobTable();
        } catch (DataBaseCreationOrOpeningException ex) {
            Logger.getLogger(JobManagerUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        

    }//GEN-LAST:event_openJobDatabaseItemActionPerformed

    private void removeSubJobButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeSubJobButtonActionPerformed

        List<MainJob> aSubJobList;
        int rowSelected;
        if (selectedMainJob == null || subJobTable.getSelectedRow() < 0) {
            ////System.out.println("No Job Selected");
        } else {
            
            if (deleteJobConfirmation() == 0) {

                aSubJobList = jobManager.getJobs(selectedMainJob.getLinkedJobs());

                rowSelected = subJobTable.getSelectedRow();
                //get the subjob and remove it from the jobDataBase
                MainJob aSubjob = aSubJobList.get(rowSelected);
                jobManager.deleteJob(aSubjob);
                selectedMainJob.getLinkedJobs().remove(rowSelected);
                jobManager.storeJob(selectedMainJob);
                refreshSubJobTable();
            }
        }
    }//GEN-LAST:event_removeSubJobButtonActionPerformed

    private int deleteJobConfirmation() throws HeadlessException {
        int confirmResult = JOptionPane.showConfirmDialog(
                this,
                "Do you really want to delete this/ these Jobs?",
                "Confirm Deletion",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.WARNING_MESSAGE);
        return confirmResult;
    }

    private void editSubJobButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editSubJobButtonActionPerformed

        List<MainJob> aSubJobList;
        MainJob aSubJob;
        int rowSelected;
        if (selectedMainJob == null || subJobTable.getSelectedRow() < 0) {
            ////System.out.println("No Job Selected");
        } else {
            aSubJobList = jobManager.getJobs(selectedMainJob.getLinkedJobs());
            rowSelected = subJobTable.getSelectedRow();
            aSubJob = aSubJobList.get(rowSelected);
            //create a new instance of JobEditorUI
            JobEditorUI jobEditorUI = new JobEditorUI(this, true, numberScheme.getClass().getCanonicalName());
            //Set its screen position from systemDefaults
            jobEditorUI.setLocation(new Integer(jobManager.systemDefaults.getProperty("jobEditorUIX")), (new Integer(jobManager.systemDefaults.getProperty("jobEditorUIY"))));
            jobEditorUI.setCurrentJobEdit(aSubJob, jobManager);
            jobEditorUI.editJob();
            jobManager.storeJob(aSubJob);
            jobManager.systemDefaults.setProperty("jobEditorUIX", new Integer(jobEditorUI.getLocation().x).toString());
            jobManager.systemDefaults.setProperty("jobEditorUIY", new Integer(jobEditorUI.getLocation().y).toString());
            refreshSubJobTable();
        }
    }//GEN-LAST:event_editSubJobButtonActionPerformed

    private void addSubJobButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addSubJobButtonActionPerformed

        

        if (selectedMainJob == null) {
            ////System.out.println("No Job Selected");
        } else {
            //get the name of the subjob from the user.
            String jobName = JOptionPane.showInputDialog(this, "Please Name The New SubJob");
            if (jobName != null) {// if a name is selected
                try {
                    // if a name is selected
                    
                    MainJob aNewSubJob = new MainJob(numberScheme.issueSubJobNumber(selectedMainJob.getJobNumber(), selectedMainJob.getLinkedJobs().size()));
                    aNewSubJob.setName(jobName);
                    //add the jobNumber to the list of jobnumbers stored in Linked Jobs of the mainJob.
                    selectedMainJob.addSubJob(aNewSubJob.getJobNumber());
                    //selectedMainJob.getLinkedJobs().add(aNewSubJob.getJobNumber());
                    jobManager.storeJob(aNewSubJob);
                    //update the main job in the database
                    jobManager.storeJob(selectedMainJob);
                    // refresh the subjob table
                    refreshSubJobTable();
                } catch (JobException ex) {
                    Logger.getLogger(JobManagerUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }


        }
    }//GEN-LAST:event_addSubJobButtonActionPerformed

    private void removeMainJobButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_removeMainJobButtonActionPerformed

        if (selectedMainJob == null) {
            ////System.out.println("No Job Selected");
        } else {
            // check with the user that these subjobs should be deleted.
            

            if (deleteJobConfirmation() == 0) {

                jobManager.deleteJob(selectedMainJob);
                selectedMainJob = null;
                refreshMainJobList();
                refreshMainJobTable();
                refreshSubJobTable();
            }
        }
    }//GEN-LAST:event_removeMainJobButtonActionPerformed

    private void editMainJobButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editMainJobButtonActionPerformed

        if (selectedMainJob == null) {
            ////System.out.println("No Job Selected");
        } else {
            //create a new instance of JobEditorUI
            JobEditorUI jobEditorUI = new JobEditorUI(this, true, numberScheme.getClass().getCanonicalName());
            //set the position of the window from default system settings.
            jobEditorUI.setLocation(new Integer(jobManager.systemDefaults.getProperty("jobEditorUIX")), (new Integer(jobManager.systemDefaults.getProperty("jobEditorUIY"))));
            //provide it with the job that is to be edited.
            jobEditorUI.setCurrentJobEdit(selectedMainJob, jobManager);

            //stop the table updater so selected mainJob is not set to null

            jobEditorUI.editJob();
            jobManager.storeJob(selectedMainJob);

            //store the position of the JobEditorUI window
            jobManager.systemDefaults.setProperty("jobEditorUIX", new Integer(jobEditorUI.getLocation().x).toString());
            jobManager.systemDefaults.setProperty("jobEditorUIY", new Integer(jobEditorUI.getLocation().y).toString());
            refreshMainJobList();
            refreshMainJobTable();
            refreshSubJobTable();
            //restart the table refresh task


        }

    }//GEN-LAST:event_editMainJobButtonActionPerformed

    private void addMainJobButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addMainJobButtonActionPerformed

        String newJobNumber = jobManager.issueJobNumber();
        MainJob newJob = new MainJob(newJobNumber);
        JobEditorUI jobEditorUI = new JobEditorUI(this, true, numberScheme.getClass().getCanonicalName());
        jobEditorUI.setCurrentJobEdit(newJob, jobManager);
        jobEditorUI.editJob();
        jobEditorUI = null;
        //jobManager.storeJob(newJob);
        selectedMainJob = null;
        refreshMainJobList();
        refreshMainJobTable();
        refreshSubJobTable();

    }//GEN-LAST:event_addMainJobButtonActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        FileOutputStream fileStream;
        try {

            //set the position of the jobmanger window in the defaults file.
            jobManager.systemDefaults.setProperty("JobMangerUIPositionX", new Integer(getLocation().x).toString());
            jobManager.systemDefaults.setProperty("JobMangerUIPositionY", new Integer(getLocation().y).toString());
            jobManager.saveDefaults();
            //save the default settings
            fileStream = new FileOutputStream("JobManDefSet.jms");
            jobManager.systemDefaults.store(fileStream, "Default System Settings");
            fileStream.close();
            jobManager.closeDatabase();
        } catch (IOException error) {
            //System.out.println(error.getMessage());
        }

    }//GEN-LAST:event_formWindowClosing

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
// TODO add your handling code here:
     /*   try{
        jobManager.systemDefaults.store(new FileOutputStream("JobManDefSet.jms"),"Default System Settings");
        jobDataBase.closeDatabase();
        }
        catch (IOException error) {
        ////System.out.println(error.getMessage());
        }*/
    }//GEN-LAST:event_formWindowClosed

    private void quickSearchButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_quickSearchButtonActionPerformed

        //create an instance of JobSearchResultsDialog
        JobSearchResultsDialog aJobSearchResultsDialog = new JobSearchResultsDialog(this, true, numberScheme.getClass().getCanonicalName());
        // create an ArrayList containing all the jobs found in a search
        ArrayList<MainJob> searchResults = new ArrayList();
        // get the search string from the text field
        String searchString = quickSearchTextField.getText();
        // ask the database to find the matching Jobs
        searchResults = new ArrayList<MainJob>(jobManager.findJob(searchString));
        // if there are no matching jobs show a dialog box stating no matches found and exit method
        if (searchResults.size() == 0) {
            JOptionPane.showMessageDialog(this, "Nothing Found With This Search Criteria");
        } else {
            // otherwise load the matching Jobs into the JobSearchResultsDialog
            aJobSearchResultsDialog.setJobList(searchResults, jobManager);
            // open the JobSearchResultsDialog
            aJobSearchResultsDialog.setVisible(true);
            // find out if any jobs have changed
            // store any changes.
        }

    }//GEN-LAST:event_quickSearchButtonActionPerformed

private void openJobAlertMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openJobAlertMenuItemActionPerformed

    jobAlerter.showAlerts();


}//GEN-LAST:event_openJobAlertMenuItemActionPerformed
    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addMainJobButton;
    private javax.swing.JButton addSubJobButton;
    private javax.swing.JMenuItem createANewJobDatabaseItem;
    private javax.swing.JButton editMainJobButton;
    private javax.swing.JButton editSubJobButton;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JButton generateReportButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jobManMainPanel;
    private javax.swing.JLabel jobManagerTitleLabel;
    private javax.swing.JLabel mainJobLabel;
    private javax.swing.JPanel mainJobListing;
    private javax.swing.JScrollPane mainJobScrollPlane;
    private javax.swing.JTable mainJobTable;
    private javax.swing.JMenuItem openJobAlertMenuItem;
    private javax.swing.JMenuItem openJobDatabaseItem;
    private javax.swing.JButton quickSearchButton;
    private javax.swing.JTextField quickSearchTextField;
    private javax.swing.JButton removeMainJobButton;
    private javax.swing.JButton removeSubJobButton;
    private javax.swing.JMenuItem replicationItem;
    private javax.swing.JRadioButtonMenuItem showCompleteRadioButton;
    private javax.swing.JRadioButtonMenuItem showUncompleteRadioButton;
    private javax.swing.JLabel subJobLabel;
    private javax.swing.JPanel subJobListing;
    private javax.swing.JScrollPane subJobScrollPlane;
    private javax.swing.JTable subJobTable;
    private javax.swing.JMenu toolsMenu;
    private javax.swing.JButton viewAlertsButton;
    private javax.swing.JMenu viewMenu;
    // End of variables declaration//GEN-END:variables

    /* InitComponets2 setup the additional UI components such as mainJobTable and subJobTable */
    private void initComponents2() {
        // Set up column sizes for the main job table
        //javax.swing.table.TableColumn column = null;
        configureMainTableColumns();
        //setup column sizes for the subJob table
        configureSubJobTableColumns();
        AddAListenerToMainJobTable();
        //setup button Group for Radiobuttons in view Menu
        ButtonGroup veiwButtonGroup = new ButtonGroup();
        //add the buttons for the View Menu
        //This button sets the list of MainJobs to those in the database that are incomplete. I.E have a percentageComplete variable of less than a 100
        veiwButtonGroup.add(showUncompleteRadioButton);
        //This button sets the list of MainJobs to those in the database that are incomplete. I.E have a percentage of 100
        veiwButtonGroup.add(showCompleteRadioButton);

    }

    private void AddAListenerToMainJobTable() {
        //add a sorter to the mainJobTable
        //mainJobTable.setAutoCreateRowSorter(true);
        //add a listener so that subJobs can be loaded from a selected mainjob
        ListSelectionModel rowSM = mainJobTable.getSelectionModel();
        rowSM.addListSelectionListener(new ListSelectionListener() {

            /** The valueChanged method reacts to changes in the mainJobTable and updates the subJobTable*/
            public void valueChanged(ListSelectionEvent e) {
                //Ignore extra messages.
                if (e.getValueIsAdjusting()) {
                    return;
                }
                //check to see if no MainJob has been selected in the JobTable
                ListSelectionModel lsm =
                        (ListSelectionModel) e.getSource();
                if (lsm.isSelectionEmpty()) {
                    //do nothing if no MainJob is selected in the JobTable
                } else {
                    //get the row in the mainJobTable that has been selected
                    int selectedRow = lsm.getMinSelectionIndex();
                    //display the list of subJobs attached to the currently selected mainJob

                    //set up some tempory variables to update the subJobTable
                    List<MainJob> subJobList;// A list of SubJobs. This will hold the
                    DefaultTableModel subJobTableModel; //The data model for the table
                    Vector<Object> subJobValues; //Vector that will store the values for each row in the subJobTable
                    Vector<Vector> jobDataValues; // This holds the rows of data in the subJobTable
                    // get the selected MainJob from the mainJobList
                    selectedMainJob = (MainJob)mainJobList.get(selectedRow);
                    // Get the model from the subJobTable
                    if (selectedMainJob == null){
                        System.out.println("Selected main job is null");
                    }
                    subJobTableModel = (DefaultTableModel) subJobTable.getModel();
                    //clear the data from the current data model by first getting the existing data
                    jobDataValues = (Vector) subJobTableModel.getDataVector();
                    //clearing the existing data
                    jobDataValues.clear();
                    //add the subJobs from the selected MainJob
                    subJobList = jobManager.getJobs(selectedMainJob.getLinkedJobs());
                    // create an Iterator to fill the subJobTable with list of SubJob information of selected MainJob
                    Iterator<MainJob> subJobIterator = subJobList.iterator();
                    //iterate through the list of SubJobs
                    while(subJobIterator.hasNext() && subJobList.size() > 0) {
                        //get the current SubJob
                        MainJob currentSubJob = subJobIterator.next();
                        //create a Vector to hold the values of the currentSubjob
                        if (currentSubJob == null){
                            System.out.println("Error retrieving subjob details");
                            return;
                        }
                        subJobValues = new Vector<Object>();
                        //add the current Subjob's name to the subJobValues
                        subJobValues.add(currentSubJob.getName());
                        //add true or false to the subJobValues
                        if (currentSubJob.getPercentComplete() == 100) {
                            subJobValues.add(true);//This ticks the job complete box for this row in the subJobTable
                        } else {
                            subJobValues.add(false);// This leaves the job complete box for this row in the subJobTable
                        }
                        jobDataValues.add(subJobValues);// add this row of data to the values in the data Model for subJobTable
                    }
                    subJobTableModel.fireTableDataChanged(); //update the UI to contain the new data from the data model
                }
            }
        });
    }

    private void configureSubJobTableColumns() {
        TableColumn column;
        for (int i = 0; i < 1; i++) {
            column = subJobTable.getColumnModel().getColumn(i);
            if (i == 0) {
                column.setPreferredWidth(210);
            } else {
                column.setPreferredWidth(30);
            }
        }
    }

    private void configureMainTableColumns() {
        TableColumn column;
        for (int i = 0; i < 3; i++) {
            column = mainJobTable.getColumnModel().getColumn(i);
            if (i == 0) {
                column.setPreferredWidth(5);
            }
            if (i == 1) {
                column.setPreferredWidth(200);
            }
            if (i == 2) {
                column.setPreferredWidth(5);
            }
            if (i == 3) {
                column.setPreferredWidth(50);
            }
        }
    }

    /**
     *This method updates the data model for the mainJobTable
     */
    private void refreshMainJobTable() {
        //ArrayList <MainJob> mainJobList;
        DefaultTableModel mainJobTableModel;// The table model for the mainJobTable
        DefaultTableModel subJobTableModel; // The table model for the subJobTable
        Vector<Object> mainJobValues; // The values in a given row of the mainJobTable
        Vector<Vector> jobDataValues; // The values in a given row of the subJobTable
        //see if there a any MainJob objects in the mainJobList
        if (mainJobList.size() == 0) {
            // If there are no MainJobs objects then empty the the models for the mainJobTable and subJobTable
            ////System.out.println("Main Job List is empty");
            // get the model for mainJobTable
            mainJobTableModel = (DefaultTableModel) mainJobTable.getModel();
            //get the data in the model
            jobDataValues = (Vector) mainJobTableModel.getDataVector();
            // Empty the Vector of data.
            jobDataValues.clear();
            // Get the model for the subJobTable
            subJobTableModel = (DefaultTableModel) subJobTable.getModel();
            // get the data for the model
            jobDataValues = (Vector) subJobTableModel.getDataVector();
            // Empty the Vector of data
            jobDataValues.clear();
            //now notifi the UI of the change in the tables
            mainJobTableModel.fireTableDataChanged();
            subJobTableModel.fireTableDataChanged();
        } //If the list of MainJobs is not empty then
        else {
            //get the table model for the mainJobTable
            mainJobTableModel = (DefaultTableModel) mainJobTable.getModel();
            //clear the current table;
            jobDataValues = (Vector) mainJobTableModel.getDataVector();// get the contents of the mainJobTable
            jobDataValues.clear();// deleate all the data in the main job table
            Iterator<Job> mainJobIterator = mainJobList.iterator();// create an iterator so that information from each job can be entered on the table
            while (mainJobIterator.hasNext()) {//iterate through the list of Mainjobs
                MainJob currentMainJob = (MainJob) mainJobIterator.next();//get a job
                mainJobValues = new Vector(); // create a new vector
                mainJobValues.add(currentMainJob.getJobNumber());//fill it with the values from the MainJob that mach the MainJobTable
                mainJobValues.add(currentMainJob.getName());
                mainJobValues.add(currentMainJob.getPercentComplete());
                mainJobValues.add(currentMainJob.getReminder());
                jobDataValues.add(mainJobValues);// add this Vector of values to the MainJobModel so that it is rendered in the table.
            }
            mainJobTableModel.fireTableDataChanged();// redraw the table
        }
    }

    /**
     *This method updates the data model for the mainJobTable
     */
    private void refreshSubJobTable() {
        List<MainJob> subJobList; // a list of SubJobs
        DefaultTableModel subJobTableModel; // the model for the subJobTable
        Vector<Object> subJobValues; // The values in a given row of the table
        Vector<Vector> jobDataValues; // A list of values that represent the rows in the subJobTable
        //See is the list of SubJobs is empty
        if (mainJobList.isEmpty()) {
            // If the list is empty then clear the data model for the subJobTable
            //get the data model for the SubJobTable
            subJobTableModel = (DefaultTableModel) subJobTable.getModel();
            //get the current Vector of values in the model
            jobDataValues = (Vector) subJobTableModel.getDataVector();
            //clear the current table;
            jobDataValues.clear();
            //tell the UI that the model has changed for subJobTable
            subJobTableModel.fireTableDataChanged();
        } //If the list of SubJobs is not empty then try to get a list of SubJobs to fill the table with
        else {
            // See if there is a mainJob selected.
            if (selectedMainJob != null) {
                //If there is a MainJob selected then get the model the model for the subJobTable
                subJobTableModel = (DefaultTableModel) subJobTable.getModel();
                //clear the current table;
                jobDataValues = (Vector) subJobTableModel.getDataVector();
                jobDataValues.clear();
                //get a list of the subJobs for the selected MainJob
                //System.out.println("subjobs to be listed are" + selectedMainJob.getLinkedJobs());
                subJobList = jobManager.getJobs(selectedMainJob.getLinkedJobs());
                //create an Instance of Iterator to fill the subJobTable with data
                Iterator<MainJob> subJobIterator = subJobList.iterator();
                // Iterate through the list of SubJobs
                while (subJobIterator.hasNext() && subJobList.size() > 0) {
                    // Get the currentSubJob for this iteration
                    MainJob currentSubJob = subJobIterator.next();
                    // Create a new Vector to store the values for this SubJob
                    subJobValues = new Vector<Object>();
                    // add the SubJob's name to the Vector
                    subJobValues.add(currentSubJob.getName());
                    // Now add the Job complete value to the Vector
                    if (currentSubJob.getPercentComplete() == 100) {
                        subJobValues.add(true);// This ticks the tickbox in the subJobTable for this row
                    } else {
                        subJobValues.add(false); //This unticks the tickbox in the subJobTable for this row
                    }
                    // the row to the model
                    jobDataValues.add(subJobValues);
                }
                // tell the UI that the model has changed.
                subJobTableModel.fireTableDataChanged();
            } else {
                // If there are no SubJobs because the selectetMainJob is null clear the table of data
                //System.out.println("There are no subjobs to be listed");
                subJobTableModel = (DefaultTableModel) subJobTable.getModel();
                //clear the current table because no job is selected;
                jobDataValues = (Vector) subJobTableModel.getDataVector();
                jobDataValues.clear();
                //tell the UI that the subJobTable has changed
                subJobTableModel.fireTableDataChanged();
            }
        }
    }

    /**
     *This method updates the mainJobList after the list of Jobs has changed.
     * This may because the MainJobs have been created or deleted.
     */
    private void refreshMainJobList() {
        if (finishedJobView == true) {
            mainJobList = new ArrayList<>(jobManager.getAllFinshedMainJobs());
        } else {
            mainJobList = new ArrayList<>(jobManager.getAllCurrentMainJobs());
        }
        //sort the mainjob list
        sortMainJobList();
    }

    /*
     *This method updates the data in the SubJobs and MainJobs 
     *if changes are made through the mainJobTable or subJobTable respectivly
     */
    @Override
    public void tableChanged(TableModelEvent e) {
        // DefaultTableModel tableModel;
        //First get the row and then the columb selected in the table.
        if (selectedMainJob == null) {
            return;
        }
        int row = e.getFirstRow();
        int column = e.getColumn();
        //////System.out.println("column " + column + " and row " + row + "have been selected");
        //See if the mainJobTable is the source of the change
        if (e.getSource() == mainJobTable.getModel()) {
            // get the model for the mainJobTable and assign it to the variable mainJobTable
            DefaultTableModel mainJobTableModel = (DefaultTableModel) mainJobTable.getModel();
            //See which column is selected and update the model accordingly
            if (column == 1) {
                //Column 1 is selected then set the jobName in the  selected MainJob to the row, column coordinates in the table model
                String newJobName = (String) mainJobTableModel.getValueAt(row, column);
                selectedMainJob.setName(newJobName);
            }
            if (column == 2) {
                //Column 2 is selected then set the percentageComplete variable in the selected MainJob to the row, column coordinates in the table model
                String percentageValueString = (String) mainJobTableModel.getValueAt(row, column);
                //parse the value to an Integer for percentageComplete setter.
                Integer percentageValue = new Integer(percentageValueString);
                selectedMainJob.setPercentComplete(percentageValue.intValue());
                //if the percentage is a 100 then set the complete date on the job to todays date
                if (percentageValue == 100) {
                    selectedMainJob.setFinishDate(new Date());
                } //if the percentage is less than 100 and a finsh date has been set then set it to null
                else {
                    if (percentageValue < 100 && selectedMainJob.getFinshDate() != null) {
                        selectedMainJob.setFinishDate(null);
                    }
                }
            }
        }
        // See if the subjobTable has been selected. if so then do the following:
        if (e.getSource() == subJobTable.getModel()) {
            // get the table model for the subJobTable and assign it to a variable
            DefaultTableModel subJobTableModel = (DefaultTableModel) subJobTable.getModel();
            // If column 0 then update the selected SubJobs jobName variable
            if (column == 0) {
                // first get the new subJob name from the table model using the row and column variables
                String newJobName = (String) subJobTableModel.getValueAt(row, column);
                //get a ArrayList of the SubJobs connected to the selectedMainJob
                List<MainJob> subJobList = jobManager.getJobs(selectedMainJob.getLinkedJobs());
                //now get the corresponding SubJob by using the row variable
                MainJob aSubJob = subJobList.get(row);
                //use the SubJob's setter for JobName to set it to the new value.
                aSubJob.setName(newJobName);
                //store the changes to the subjob to the database
                jobManager.storeJob(aSubJob);
            }
            // If Column 1 is selected set the percentComplete variable on the corresponding SubJob to 0 or 100
            if (column == 1) {
                //get the boolean value from the subTableModel using the row and column variables
                Boolean jobComplete = (Boolean) subJobTableModel.getValueAt(row, column);
                //get an ArrayList of Subjobs from the selectedMainJobVariable
                List<MainJob> subJobList = jobManager.getJobs(selectedMainJob.getLinkedJobs());
                //get the correct SubJob by using the row variable
                MainJob aSubJob = subJobList.get(row);
                //find out if the jobComplete variable is true
                if (jobComplete.equals(true)) {
                    //set the percentComplete variable on the SubJob to 100
                    aSubJob.setPercentComplete(100);
                    //////System.out.println("subJob Completed");
                    //set the finish date for the job.
                    aSubJob.setFinishDate(new Date());
                    //set the jobStarted to todays date if null
                    if (aSubJob.getstartDate() == null) {
                        aSubJob.setstartDate(new Date());
                    }
                } else {
                    //If the jobComplete variable is false:
                    if (jobComplete.equals(false)) {
                        //Set the PercentComplete variable to 0
                        aSubJob.setPercentComplete(0);
                        //////System.out.println("subJob Not Completed");
                        aSubJob.setFinishDate(null);
                        //////System.out.println("subJob Not Completed");
                    } else {////System.out.println("no column selected");
                        ////System.out.println("column selected is " + column);
                    }
                }
                //store the changes to the subjob to the database
                jobManager.storeJob(aSubJob);
            }
        }
        // now the changes hav been made to the models then record the changes in persistent storage if it is not null
        if (selectedMainJob != null) {
            jobManager.storeJob(selectedMainJob);
        }
        //update the jobList
        refreshMainJobList();
        //refreshMainJobTable();
        //refreshSubJobTable();
    }

    /*
     * This method stores/ updates the MainJob referrenced by selectedMainJob to persistant storage and then updates the
     * mainJobList, mainJobTable and subJobTables accordingly
     */
    public void storeCurrentJobEdit() {
        // store the currentJobEdit to persistent storage
        jobManager.storeJob(selectedMainJob);
        //update the mainJobList from persistent storage
        refreshMainJobList();
        //update the mainJobTable based on the mainJobList
        refreshMainJobTable();
        // update the subJobTable based on the selectedMainJob variable
        refreshSubJobTable();
        //update the jobAlerter
        jobAlerter.setMainJobList(jobManager.getAllCurrentMainJobs());
    }

    private void sortMainJobList() {try {
        //This method puts the list in order of jobNumber
        Comparator comparitor = (Comparator) Class.forName(numberScheme.getComparator()).newInstance();
        Collections.sort(mainJobList, comparitor);
        //mainJobList
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            Logger.getLogger(JobManagerUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    class RefreshTableTask extends TimerTask {

        @Override
        public void run() {
            //update the mainJobTable based on the mainJobList
            refreshMainJobTable();
            // update the subJobTable based on the selectedMainJob variable
            refreshSubJobTable();
            //selectedMainJob = null;
        }
    }
}
