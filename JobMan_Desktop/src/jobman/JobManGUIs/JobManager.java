/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package jobman.JobManGUIs;

import JobTypes.BaseTypes.Job;
import jobman.database.JobDataBaseDB4OEmbedded;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import jobman.JobException;
import JobTypes.MainJob;
import JobTypes.BaseTypes.SpawnableJob;
import jobman.database.DataBaseCreationOrOpeningException;

/**
 *
 * @author Robert Worrall
 */
public class JobManager {
//Variables for the class

    public Properties systemDefaults = new Properties();// handles the storage and retrieval of default settings such as current database etc
    private final JobDataBaseDB4OEmbedded jobDataBase = new JobDataBaseDB4OEmbedded();
    private final SettingsGUI settingsGUI = new SettingsGUI();
    private final Timer replicateTimer = new Timer(true); // timer to manage replaction tasks


    public JobManager() throws DataBaseCreationOrOpeningException {
        openDefaultFile();
        //set the posision of the alert dialog box
        openDefaultDB();
        checkReplcatableJobs task3 = new checkReplcatableJobs();
        replicateTimer.scheduleAtFixedRate(task3, 1020, 50000);
        
    }

    /**
     * Code for retrieving default system settings. These settings include the
     * location of where the last accessed database of MainJobs is loacated.
     */
    private void openDefaultFile() {

        systemDefaults = settingsGUI.getSystemDefaultFile();

    }

    /**
     * This method to open the default database file that contains the MainJobs
     */
    private void openDefaultDB() throws DataBaseCreationOrOpeningException {
        String defaultDBlocation;// This String is the address of the default database. This is the last database to be opened by the application
        File openFile; // File object for the database being worked on

        //Check the defaultDatabase property from systemDefaults.
        defaultDBlocation = settingsGUI.checkDefaultDatabase(systemDefaults);
        System.out.println("opening database: " + defaultDBlocation);
        //if new database is returned then create new database
        if (settingsGUI.newDataBase()) {
            createDatabase(defaultDBlocation);
        }
        // now open the database for editting
        openFile = new File(defaultDBlocation);
        //confirm that the database can be read.
        if (openFile.canRead() == true) {
            //if the above is true. Open the database
            //System.out.println("opening database");
            loadDatabase(defaultDBlocation);
        } else {
            //if the file cannot be opened then set the defaultDatabase proprty to none and rerun this method
            systemDefaults.setProperty("defaultDataBase", "none");
            openDefaultDB();
        }
    }

    public void createANewJobDatabase() throws DataBaseCreationOrOpeningException {

        File newFile = settingsGUI.createNewDataBase();
        String defaultDBlocation = newFile.getPath();

        closeDatabase();
        createDatabase(defaultDBlocation);
        loadDatabase(defaultDBlocation);
        systemDefaults.setProperty("defaultDataBase", defaultDBlocation);
        systemDefaults.setProperty("numberingScheme", "jobman.database.jobnumberingschemes.SimpleJobNumberingScheme");
    }

    public void openJobDataBase() throws DataBaseCreationOrOpeningException {

        //System.out.println("Opening another Database");
        File openFile = settingsGUI.openJobDataBase();

        if (openFile.canRead() == true) {
            closeDatabase();

            loadDatabase(openFile.getPath());
            systemDefaults.setProperty("defaultDataBase", openFile.getPath());
        } else {
            systemDefaults.setProperty("defaultDataBase", "none");
            openDefaultDB();

        }
    }

    public Set<MainJob> getAllCurrentMainJobs() {
        return jobDataBase.getAllCurrentMainJobs();
    }

    public Set<SpawnableJob> getReplicatableJobs() {
        return jobDataBase.getReplicatableJobs();
    }

    public void deleteReplicableJob(SpawnableJob replicatableJob) {
        jobDataBase.deleteReplicableJob(replicatableJob);
    }

    public void storeReplicatableJob(SpawnableJob replicatableJob) {
        jobDataBase.storeReplicatableJob(replicatableJob);
    }

    public void closeDatabase() {
        jobDataBase.closeDatabase();
    }

    private void createDatabase(String DBlocation) throws DataBaseCreationOrOpeningException {       
        jobDataBase.createDatabase(DBlocation, systemDefaults.getProperty("numberingScheme"));
    }

    private void loadDatabase(String defaultDBlocation) {
        //System.out.println("Opening database" + defaultDBlocation);
        jobDataBase.loadDatabase(defaultDBlocation);
    }

    public synchronized void storeJob(Job aMainJob) {
        jobDataBase.storeJob(aMainJob);
    }

    public void deleteJob(MainJob aMainJob) {
        //get a list of the subjobs and delete them
        List<MainJob> subjobsToBeDeleted = getJobs(aMainJob.getLinkedJobs());
        // delete the top level job
        jobDataBase.deleteJob(aMainJob);
        // delete all the subjobs
        for (MainJob asubJob : subjobsToBeDeleted) {
            deleteJob(asubJob);
        }
    }

    public String issueJobNumber() {
        return jobDataBase.issueJobNumber();
    }

    public Set findJob(String searchString) {
        return jobDataBase.findJob(searchString);
    }

    public Set<Job> getAllFinshedMainJobs() {
        return jobDataBase.getAllFinshedMainJobs();
    }

    public MainJob findMainJobByJobNumber(String aMainJob) {
        return jobDataBase.findJobByJobNumber(aMainJob);
    }

    public ArrayList<String> getCatagoryList() {
        return jobDataBase.getCatagoryList();
    }

    

    public void saveDefaults() {

    }

    public List<MainJob> getJobs(ArrayList<String> linkedJobs) {
        List<MainJob> result = new ArrayList();
        for (String subJobName : linkedJobs) {
            result.add(jobDataBase.findJobByJobNumber(subJobName));
        }
        return result;
    }

    /**
     * This method retrieves the length of time spent on a given job.
     *
     * @param jobNumber
     * @return length of time spent on Job
     */
    public long getTimeSpentOnJob(String jobNumber) {
        List<MainJob> jobList;
        MainJob topLevelJob = findMainJobByJobNumber(jobNumber);
        // see if this is null if so return 0
        if (topLevelJob == null) {
            return 0;
        } else {
            jobList = getJobs(topLevelJob.getLinkedJobs());
            long timeSpent = 0;
            //System.out.println("Time spent on Job " + topLevelJob.getJobNumber() + " is " + topLevelJob.getTimeSpent());
            timeSpent = timeSpent + topLevelJob.getTimeSpent();
            //now get the timespent on all the subjobs
            timeSpent = timeSpent + getTimeSpentOnSubjobs(jobList);
            return timeSpent;
        }

    }

    /**
     * This method resets the timespent on a given job and all it's subjobs.
     *
     * @param jobNumber
     * @return timespent
     */
    public boolean resetTimeSpentOnJob(String jobNumber) {
        boolean resetCompleted = false;
        List<String> jobList;
        MainJob topLevelJob = findMainJobByJobNumber(jobNumber);
        if (topLevelJob == null) {
            return false;
        } else {
            //reset the time on the top level job.
            topLevelJob.incrementTimeSpent(0);
            //store it to the database.
            jobDataBase.storeJob(topLevelJob);
            //now find any subjobs and reset the time spent on them.
            if (topLevelJob.getLinkedJobs().isEmpty()) {
                // there are no subjobs to reset
                resetCompleted = true;
            } else {
                resetCompleted = true;
                //get the list of subjobs and iterate through them resetting the time spent on each
                jobList = topLevelJob.getLinkedJobs();
                for (String aSubJob : jobList) {
                    if (resetTimeSpentOnJob(aSubJob) == false) {
                        resetCompleted = false;
                    }// catch if any of the subjobs are note reset
                }
            }
        }
        return resetCompleted;
    }

    private long getTimeSpentOnSubjobs(List<MainJob> jobList) {
        long timeSpent = 0;
        for (MainJob job : jobList) {
            //add the time spent at this level
            timeSpent = timeSpent + job.getTimeSpent();
            //get a list of subjobs.
            List<MainJob> subJobList = getJobs(job.getLinkedJobs());
            //now find the time spent on the subjobs
            timeSpent = timeSpent + getTimeSpentOnSubjobs(subJobList);
        }

        return timeSpent;
    }

    /*
     *Inner Class that extends a TimerTask. It retrieves a List of MainJobReplicators that are due to be replicated and replicates them.
     */
    class checkReplcatableJobs extends TimerTask {

        //MainJobReplicator aJob; // A given MainJobReplicator that is to be examined.
        Job aNewMainJob; // A MainJob that is replcated from a MainJobReplicator
        List<Job> replicatedSubJobs;
        Date replicationDate; // A given date that a MainJobReplicator is due to be replicated
        Date now;  // The date the class is created
        boolean replicateWhenJobCompletes; // A variable that tells the class to replicate a given MainJobReplicator when it's corresponding MainJob is completed.

        @Override
        public void run() {
            // First get the date and time at present
            now = new Date();
            // Now retrieved an ArrayList of MainJobReplcators from the database.
            //System.out.println("Checking jobs that require replication");
            ArrayList<SpawnableJob> replicateList = new ArrayList<SpawnableJob>(getReplicatableJobs());// An ArrayList of MainJobReplicators to be examined to see if any of them are due to be replicated.
            checkReplicationList(replicateList);
        }

        private void checkReplicationList(ArrayList<SpawnableJob> replicateList) {
            // Create an Iterator to go through the list of MainJobReplicators
            Iterator<SpawnableJob> replicateIterator = replicateList.iterator();
            // Go through the list of MainJobReplicators and replicat new MainJobs where needed.
            while (replicateIterator.hasNext()) {
                // get the next MainJobReplicator from the List
                SpawnableJob aJob = replicateIterator.next();
                //System.out.println("Checking Job " + aJob.getJobName());
                // establish if the spawn when job completes is true and date to spawn is set to null
                if (checkJobIsToSpawnWhenCompletedAndIfSpawnDateIsSetToNull(aJob)) {
                    //System.out.println("Job replcates when existing job completes");
                    //if so then set the job to be spawned after the replication period
                    String relatedJob = aJob.getExistingJobNumber();
                    //see if the existing Job has been set
                    if (relatedJob == null) {
                        //System.out.println("related job is null");
                        //if not set date to spawn to the replication period as the existing job does not exist to tell us when it was completed
                        long newDateToSpawn = now.getTime() + aJob.getReplicationPeriod();
                        aJob.setDateToSpawn(new Date(newDateToSpawn));
                        //System.out.println("set Job to spawn at " + aJob.getDateToSpawn());
                    } //if the related Job name is not set to null then try to retrieve the job.
                    else {
                        boolean jobActive = CheckJobActive(relatedJob);
                        //see if the job is active. if so do nothing
                        if (jobActive == true) {
                            // job is active so don't set a date to replacate
                        } else {
                            setReplicationDate(relatedJob, aJob);
                        }
                    }
                    //now see if the job is due to be replicated.
                    //System.out.println("See if Job needs to be replicated now");
                } else {
                    CheckIfJobNeedsToSpawnAndIfSoSpawn(aJob);
                }
                //now update the replicatableMainJob in persistent storage to cover any changes
                //System.out.println("Update replicatable job in the database");
                storeReplicatableJob(aJob);
            }
        }

        private void setReplicationDate(String relatedJob, SpawnableJob aJob) {
            //System.out.println("current Job is completed");
            //The job is not active so look in the database to see if it can be found
            MainJob currentJob = findMainJobByJobNumber(relatedJob);
            //check to see if null is returned
            if (currentJob == null) {
                //System.out.println("releated Job could not be retrieved");
                //if so set date to spawn to the replication period as the existing job does not exist to tell us when it was completed
                long newDateToSpawn = now.getTime() + aJob.getReplicationPeriod();
                //System.out.println("Setting replication date to " + newDateToSpawn);
                aJob.setDateToSpawn(new Date(newDateToSpawn));
            } else {
                //System.out.println("see if the Job has finished date");
                Date finishDate = currentJob.getFinshDate();
                if (finishDate != null) {
                    // if the finish date is not null set the replication date to the replication period after the job was finished
                    //System.out.println("Finish date set. Set Replication period from finish date");
                    long newDateToSpawn = finishDate.getTime() + aJob.getReplicationPeriod();
                    aJob.setDateToSpawn(new Date(newDateToSpawn));
                } else {
                    // if the finish date is null set the replication date to the replication period after today
                    //System.out.println("No finish date set. Set finish date to now plus replication period");
                    long newDateToSpawn = now.getTime() + aJob.getReplicationPeriod();
                    aJob.setDateToSpawn(new Date(newDateToSpawn));
                }
            }
        }

        private boolean CheckJobActive(String relatedJob) {
            //System.out.println("See if related job has completed");
            //first see if the job is still active I.E not complete
            boolean jobActive = false;// boolean to test if job is active
            //get a list of the current main Jobs
            Set<MainJob> mainJobList = getAllCurrentMainJobs();
            Iterator<MainJob> jobListIterator = mainJobList.iterator();
            //iterate through the list if mainJobs to find the related job
            while (jobListIterator.hasNext()) {
                MainJob currentJob = jobListIterator.next();
                //if the currentJob viewed has the same job number then the jobactive is set to true does nothing else
                if (currentJob.getJobNumber().equals(relatedJob)) {
                    //System.out.println("found related job see if the job is active");
                    if (currentJob.getPercentComplete() != 100) {
                        //System.out.println("current job is not complete");
                        jobActive = true;
                    }

                }
            }
            return jobActive;
        }

        private boolean checkJobIsToSpawnWhenCompletedAndIfSpawnDateIsSetToNull(SpawnableJob aJob) {
            return aJob.isSpawnWhenJobCompletes() && (aJob.getDateToSpawn() == null);
        }

        private void CheckIfJobNeedsToSpawnAndIfSoSpawn(SpawnableJob aJob) {
            // get the date it is due to replicate
            replicationDate = aJob.getDateToSpawn();
            // compare the replication date to the time now
            if (replicationDate.getTime() <= now.getTime()) {
                try {
                    //System.out.println("Job is due to be replicated");
                    //if the replication time has passed spawn a new MainJob
                    if (aJob.spawn()) {
                        //System.out.println("New job has been successfully spawned");
                        aNewMainJob = aJob.getLastSpawnedJob();
                        //now get list of spawned subjobs
                        replicatedSubJobs = aJob.getSpawnedSubjobs();
                        // get a new job number from the JobDataBaseDB4OEmbedded instance
                        aNewMainJob.setJobNumber(issueJobNumber()); //get a new jobnumber for this spawned Job
                        //store the job in the database
                        //System.out.println("Storing the new job " + aNewMainJob.getJobName() + " Job number: " + aNewMainJob.getJobNumber());
                        storeJob(aNewMainJob);
                        //store the subjobs into the database
                        if (replicatedSubJobs == null || replicatedSubJobs.isEmpty()) {
                            // do not bother try to iterate through the list
                        } else {
                            for (Job aSubjob : replicatedSubJobs) {
                                //System.out.println("Storing SubJob " + aSubjob.getJobName());
                                storeJob(aSubjob);
                            }
                        }
                        //alter the date the MainJobReplicator replicates to the given period specified in replicationPeriod variable of the said instance
                        if (aJob.isSpawnWhenJobCompletes()) {
                            //System.out.println("Job Spawns when it completes setting date for next spawn to null");
                            aJob.setDateToSpawn(null);
                            aJob.setExistingJobNumber(aNewMainJob.getJobNumber());
                        } else {
                            long newDateToSpawn = now.getTime() + aJob.getReplicationPeriod();
                            //System.out.println("Setting next replication to " + newDateToSpawn);
                            aJob.setDateToSpawn(new Date(newDateToSpawn));
                        }
                    }
                } catch (JobException ex) {
                    Logger.getLogger(JobManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            } else {
                //System.out.println("Job is not due to be replicated");
            }
        }
    }
}
